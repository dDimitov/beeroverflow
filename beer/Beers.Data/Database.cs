﻿using Beers.Models;
using System.Collections.Generic;

namespace Beers.Data
{
    public static class Database
    {
        static Database()
        {
            Beers = new List<Beer>();
            Breweries = new List<Brewery>();

            SeedData();
        }

        public static List<Beer> Beers { get; set; }
        public static List<Brewery> Breweries { get; set; }
        public static List<User> Users { get; set; }
        public static List<Country> Countries { get; set; } = new List<Country>();
        public static List<Style> Styles { get; set; }

        private static void SeedData()
        {
            Countries.AddRange(new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria"
                },
                new Country
                {
                    Id = 2,
                    Name = "England"
                }
            }
            );
            Beers.AddRange(new List<Beer>
            {
                new Beer
                {
                    Id = 1,
                    Name = "London Pride",
                    Description = "It’s a beautiful thing to be able to pour your own pint of Pride - and this 5 litre mini-keg, keeps the beers    flowing freely. It holds no fewer than 8 pints of our revered London Pride and represents the ultimate way to enjoy the    nation’s favourite ale at home.",
                    BreweryId = 1,
                    ABV = 4.1
                },
                new Beer
                {
                    Id = 2,
                    Name = "Frontier",
                    Description = "Frontier is the first of its kind for Fuller’s, a craft lager that pushes the boundaries to deliver  refreshment, but not at the expense of flavour. Crafted for 42 days, using a blend of new world hops and old world brewing    techniques for a memorable tasting beer.",
                    BreweryId = 1,
                    ABV = 4.2
                },
                new Beer
                {
                    Id = 3,
                    Name = "Honey Dew",
                    Description = "Fuller’s Organic Honey Dew is an award-winning, refreshingly golden beer bursting with subtle honey flavours.    Brewed using organic English malts and the finest organic honey, it has a sweet, mellow, rounded character, with a     delicious zesty edge from the English organic hops. Get this great organic beer delivered today!",
                    BreweryId = 1,
                    ABV = 4.8
                }
            });
        }
    }
}
