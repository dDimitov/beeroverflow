﻿using Beers.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Beers.Data
{
    public static class DataExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var data = LoadData();

            var countries = CreateCountryModels(data);
            modelBuilder.Entity<Country>().HasData(countries);

            var styles = CreateStyleModels(data);
            modelBuilder.Entity<Style>().HasData(styles);

            var breweries = CreateBreweryModels(data, countries);
            modelBuilder.Entity<Brewery>().HasData(breweries);

            var beers = CreateBeerModels(data, styles, breweries);
            modelBuilder.Entity<Beer>().HasData(beers);
        }

        private static List<Dictionary<string, string>> LoadData()
        {
            var beers = new List<Dictionary<string, string>>();
            var path = @"./Seed/Beers.csv";
            //var path = @"/Users/Valery/Documents/Telerik/BeerOverflow/beeroverflow/beer/Beers.Web/Seed/Beers.csv";
            using (TextFieldParser parser = new TextFieldParser(path))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");

                while (!parser.EndOfData)
                {
                    var beer = new Dictionary<string, string>();
                    string[] fields = parser.ReadFields();

                    beer.Add("Name", fields[0]);
                    beer.Add("ABV", fields[1]);
                    beer.Add("Style", fields[2]);
                    beer.Add("Brewery", fields[3]);
                    beer.Add("Country", fields[4]);

                    beers.Add(beer);
                }
            }

            return beers;
        }

        private static List<Country> CreateCountryModels(List<Dictionary<string, string>> data)
        {
            var countryNames = GetUniqueColumnValues(data, column: "Country");

            var countries = new List<Country>();
            var idCounter = 1;

            foreach (var name in countryNames)
            {
                countries.Add(new Country() { Id = idCounter++, Name = name });
            }

            return countries;
        }

        private static List<Style> CreateStyleModels(List<Dictionary<string, string>> rawBeers)
        {
            var uniqueStyles = GetUniqueColumnValues(rawBeers, column: "Style");

            var styles = new List<Style>();
            var counter = 1;

            foreach (var name in uniqueStyles)
            {
                styles.Add(new Style() { Id = counter++, Name = name });
            }

            return styles;
        }

        private static List<Brewery> CreateBreweryModels(List<Dictionary<string, string>> rawBeers, List<Country> countries)
        {
            var uniqueBreweryNames = new HashSet<string>();
            var breweries = new List<Brewery>();
            var idCounter = 1;

            foreach (var beer in rawBeers)
            {
                if (!uniqueBreweryNames.Contains(beer["Brewery"]))
                {
                    var country = countries.Where(country => country.Name == beer["Country"]).First();
                    var brewery = new Brewery()
                    {
                        Id = idCounter++,
                        Name = beer["Brewery"],
                        CountryId = country.Id,
                    };

                    breweries.Add(brewery);

                    uniqueBreweryNames.Add(beer["Brewery"]);
                }
            }

            return breweries;
        }

        private static List<Beer> CreateBeerModels(List<Dictionary<string, string>> rawBeers, List<Style> styles, List<Brewery> breweries)
        {
            var beers = new List<Beer>();
            var counter = 1;

            foreach (var rawBeer in rawBeers)
            {
                var style = styles.Where(style => style.Name == rawBeer["Style"]).First();
                var brewery = breweries.Where(brewery => brewery.Name == rawBeer["Brewery"]).First();
                var abv = double.Parse(rawBeer["ABV"]);
                //rawBeer["ABV"];

                var beer = new Beer()
                {
                    Id = counter,
                    Name = rawBeer["Name"],
                    ABV = Math.Round(abv, 1),
                    StyleId = style.Id,
                    BreweryId = brewery.Id,
                };

                beers.Add(beer);
                counter++;
            }

            return beers;
        }

        private static HashSet<string> GetUniqueColumnValues(List<Dictionary<string, string>> table, string column)
        {
            var uniqueValues = new HashSet<string>();

            foreach (var row in table)
            {
                uniqueValues.Add(row[column]);
            }

            return uniqueValues;
        }
    }
}
