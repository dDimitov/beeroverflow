﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Beers.Data.Migrations
{
    public partial class drunk4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReviewLikes");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_Reviews_BeerReviewId",
                table: "Reviews");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Reviews",
                table: "Reviews");

            migrationBuilder.DropIndex(
                name: "IX_Reviews_BeerId",
                table: "Reviews");

            migrationBuilder.DropColumn(
                name: "BeerReviewId",
                table: "Reviews");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Reviews",
                table: "Reviews",
                columns: new[] { "BeerId", "UserId" });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "ca5424e7-3404-42ca-9f24-cf77bcc8e9a4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "69a840ac-55f6-4bc4-bd27-ca7643709c4f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e2e717b9-a964-469f-9f2e-2b27b6ccc2ac", "AQAAAAEAACcQAAAAELWPhSs1LNfcRKP5dtjLQO/1cC8bosSYc+wOtBfbr3WYB+yABYRwS0s9o2EtQQKw8A==", "16c317f8-8f4f-4c97-9630-7ef3c0c8d9e5" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Reviews",
                table: "Reviews");

            migrationBuilder.AddColumn<int>(
                name: "BeerReviewId",
                table: "Reviews",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Reviews_BeerReviewId",
                table: "Reviews",
                column: "BeerReviewId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Reviews",
                table: "Reviews",
                columns: new[] { "BeerReviewId", "BeerId", "UserId" });

            migrationBuilder.CreateTable(
                name: "ReviewLikes",
                columns: table => new
                {
                    BeerReviewId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Like = table.Column<int>(type: "int", nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReviewLikes", x => new { x.BeerReviewId, x.UserId });
                    table.ForeignKey(
                        name: "FK_ReviewLikes_Reviews_BeerReviewId",
                        column: x => x.BeerReviewId,
                        principalTable: "Reviews",
                        principalColumn: "BeerReviewId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReviewLikes_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "7ab84e4d-9b43-486f-892c-ca7f1ea5ab1e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "9aac793b-a769-4360-90fb-d35605171164");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "8bf127a1-a030-4d48-b758-78b3c8f88629", "AQAAAAEAACcQAAAAEPidiximcezZ8HS8ZCW9ofvgtMWjxIVMfxqKbenG7ycU1rPTSHcDtabL3mgOajRZZQ==", "2205f537-9de9-404a-b737-3bed93ad9e5a" });

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_BeerId",
                table: "Reviews",
                column: "BeerId");

            migrationBuilder.CreateIndex(
                name: "IX_ReviewLikes_UserId",
                table: "ReviewLikes",
                column: "UserId");
        }
    }
}
