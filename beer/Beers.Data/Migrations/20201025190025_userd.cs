﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Beers.Data.Migrations
{
    public partial class userd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "03b2b954-eeca-4cec-a2f5-66964777f0cb");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "671912e4-d682-47fd-be48-4e46a3639096");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2e8098e4-4071-46af-8e94-d62535e136b3", "AQAAAAEAACcQAAAAEJVm0BI6/e8av5e7dHBOEb5A8i+i7oKRhkPfLon1k9J/tuTeosDcVWnUNhcXeJ+czQ==", "8909c1cc-a363-46ec-85c9-f4c364e3286e" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "508c09ab-1468-44bb-813a-6e088543b8a6");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "801f7ee2-3452-465d-a5c5-2871cfaa1d2e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "65f4ef98-c49f-4ecd-999b-6bf73786a5ce", "AQAAAAEAACcQAAAAEIt9BUI37eABjLsyCLQ3f/5AmNzPpgCBJsERYE/AOvuSW2+kW5sKChv/GuacLOiynQ==", "eefecc94-192b-4b32-98c4-e3fd3d25ea2d" });
        }
    }
}
