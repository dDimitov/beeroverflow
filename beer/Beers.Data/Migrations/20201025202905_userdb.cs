﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Beers.Data.Migrations
{
    public partial class userdb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "78b992c6-e355-4513-80f1-86cd6cb8cecd");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "3dc4867d-fb01-4a4f-b859-60512ad76205");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "d3330fe9-ac5a-44c4-a213-cc375b2e2390", "AQAAAAEAACcQAAAAEDCUGaWixu9XHTgeDINX04CnttqY7aB6HJpSqZjQVyzzQZeo2dadx1DkN8J3/oZmbQ==", "133dd944-0997-464b-b8e0-9f294ef8f2f8" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "03b2b954-eeca-4cec-a2f5-66964777f0cb");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "671912e4-d682-47fd-be48-4e46a3639096");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2e8098e4-4071-46af-8e94-d62535e136b3", "AQAAAAEAACcQAAAAEJVm0BI6/e8av5e7dHBOEb5A8i+i7oKRhkPfLon1k9J/tuTeosDcVWnUNhcXeJ+czQ==", "8909c1cc-a363-46ec-85c9-f4c364e3286e" });
        }
    }
}
