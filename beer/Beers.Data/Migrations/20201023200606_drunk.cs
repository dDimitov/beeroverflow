﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Beers.Data.Migrations
{
    public partial class drunk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "2ace5452-e2cc-42bf-81d0-8ac313f45353");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "0672b973-7379-4ce2-8e82-fca05f0f294f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "ddd4df74-0b73-440e-a70c-e723c8694741", "AQAAAAEAACcQAAAAEGVjYioxlZb7BPJk4K0QKjFMUIAwR46ae+haO+XUnp1JJaiyM9VKusydqjs5j+oIUw==", "9075c148-8f30-46d2-a218-cf00069c65cc" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "03e85cef-6e20-4fce-8a17-e264031e118b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "b1e419fe-8947-40b7-aa54-4b013d6bd037");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "68a0a4ce-fae9-4713-9bb3-bf20ba665fe1", "AQAAAAEAACcQAAAAECavqok5On9+IL22C5S79w5/0FRmI+jkaUglq7PvSfUVJ1yPBx+njYPbS6lwM1iVnQ==", "733c0e95-2511-4ae9-913f-81c799f8d4c3" });
        }
    }
}
