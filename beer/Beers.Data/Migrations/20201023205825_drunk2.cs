﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Beers.Data.Migrations
{
    public partial class drunk2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Rating",
                table: "Reviews",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "a715e888-e531-42ae-ab39-11663a5bda40");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "f21143c1-7169-4d5a-aea0-81b252cf2eaf");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "325afed6-5008-4606-aa09-794171f233ca", "AQAAAAEAACcQAAAAEHAT5pP7YECAzl2Md41mmkLUHumJdxUwreUyK103PkoG6QuiOJxKC9pqRS3BXHRPnQ==", "be602403-d0b9-4f4e-9df2-e53c41e49d01" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rating",
                table: "Reviews");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "2ace5452-e2cc-42bf-81d0-8ac313f45353");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "0672b973-7379-4ce2-8e82-fca05f0f294f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "ddd4df74-0b73-440e-a70c-e723c8694741", "AQAAAAEAACcQAAAAEGVjYioxlZb7BPJk4K0QKjFMUIAwR46ae+haO+XUnp1JJaiyM9VKusydqjs5j+oIUw==", "9075c148-8f30-46d2-a218-cf00069c65cc" });
        }
    }
}
