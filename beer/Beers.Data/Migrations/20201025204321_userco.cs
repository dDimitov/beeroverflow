﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Beers.Data.Migrations
{
    public partial class userco : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "c91345c4-1655-4105-b6e7-0670aa2a693a");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "d6d3af28-74c0-47b5-9a1e-5c7385e56ff6");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2fe1aa15-8fbb-4bce-896e-b8e2d1331e71", "AQAAAAEAACcQAAAAEBiCDMdjpqt8pHacggaxq+gjeBjAqQxef61GrNhrdFd6asay6e/rtBCFhnLsx/j+cw==", "62558cec-d20c-45b3-952c-fd17c90808bc" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "78b992c6-e355-4513-80f1-86cd6cb8cecd");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "3dc4867d-fb01-4a4f-b859-60512ad76205");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "d3330fe9-ac5a-44c4-a213-cc375b2e2390", "AQAAAAEAACcQAAAAEDCUGaWixu9XHTgeDINX04CnttqY7aB6HJpSqZjQVyzzQZeo2dadx1DkN8J3/oZmbQ==", "133dd944-0997-464b-b8e0-9f294ef8f2f8" });
        }
    }
}
