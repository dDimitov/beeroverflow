﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Beers.Data.Migrations
{
    public partial class rel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "03e85cef-6e20-4fce-8a17-e264031e118b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "b1e419fe-8947-40b7-aa54-4b013d6bd037");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "68a0a4ce-fae9-4713-9bb3-bf20ba665fe1", "AQAAAAEAACcQAAAAECavqok5On9+IL22C5S79w5/0FRmI+jkaUglq7PvSfUVJ1yPBx+njYPbS6lwM1iVnQ==", "733c0e95-2511-4ae9-913f-81c799f8d4c3" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "492a7527-91e2-4911-a3dd-658c668a03c0");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "e37581b9-3ef2-4fd7-87da-042150dcf480");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "28fdad1a-e644-4703-a092-6db12aff77b3", "AQAAAAEAACcQAAAAEHL0OyGyHRqyyhRawpspeiyQqx4VVPFMRUMeI+XttT6OtjPv8WYd1DI3MZ0FetopSQ==", "49a6e059-2200-42a7-9acc-b31ff1afc9d5" });
        }
    }
}
