﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Beers.Data.Migrations
{
    public partial class Initial3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "492a7527-91e2-4911-a3dd-658c668a03c0");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "e37581b9-3ef2-4fd7-87da-042150dcf480");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "28fdad1a-e644-4703-a092-6db12aff77b3", "AQAAAAEAACcQAAAAEHL0OyGyHRqyyhRawpspeiyQqx4VVPFMRUMeI+XttT6OtjPv8WYd1DI3MZ0FetopSQ==", "49a6e059-2200-42a7-9acc-b31ff1afc9d5" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "fdcefb97-6a35-48c5-bf05-4bd6da0d16ce");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "9eb099ec-7942-4fe3-a27e-4ec3fd24e4e2");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "6b726a1b-2bb5-46c0-b788-3141c712ab45", "AQAAAAEAACcQAAAAEAg1vBDYTPTwdsLDhTkU7RHRqO6pvogPG4on/m33V9CHBSzKd3Mxng5Y7nYGTUZV7Q==", "1144149d-136a-42cf-983a-b87c009ea82e" });
        }
    }
}
