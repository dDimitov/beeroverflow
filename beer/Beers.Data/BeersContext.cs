﻿using Beers.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Beers.Data
{
    public class BeersContext : IdentityDbContext<User, IdentityRole<int>, int>
    {
        public BeersContext(DbContextOptions<BeersContext> options) : base(options)
        {
        }

        public DbSet<Beer> Beers { get; set; }
        public DbSet<Brewery> Breweries { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Style> Styles { get; set; }
        public virtual DbSet<BeerRating> Ratings { get; set; }
        public virtual DbSet<BeerReview> Reviews { get; set; }
        public virtual DbSet<WishListItem> WishLists { get; set; }
        //public virtual DbSet<ReviewLike> ReviewLikes { get; set; }
        public virtual DbSet<AlreadyDrunkBeers> AlreadyDrunkBeers { get; set; }
        //public DbSet<User> Users { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // roles
            modelBuilder.Entity<Role>().HasData(
                new Role() { Id = 1, Name = "Admin", NormalizedName = "ADMIN" },
                new Role() { Id = 2, Name = "User", NormalizedName = "USER" }
            );

            // admin account
            var hasher = new PasswordHasher<User>();
            var adminUser = new User();
            adminUser.Id = 1;
            adminUser.UserName = "admin@admin.com";
            adminUser.Name = "admin@admin";
            adminUser.NormalizedUserName = "ADMIN@ADMIN.COM";
            adminUser.Email = "admin@admin.com";
            adminUser.NormalizedEmail = "ADMIN@ADMIN.COM";
            adminUser.PasswordHash = hasher.HashPassword(adminUser, "admin123");
            adminUser.SecurityStamp = Guid.NewGuid().ToString();
            modelBuilder.Entity<User>().HasData(adminUser);
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>()
                {
                    RoleId = 1,
                    UserId = adminUser.Id
                }
            );

            // configure entity relations
            // modelBuilder.Entity<BeerRating>().HasKey(e => new { e.BeerId, e.UserId });
            //modelBuilder.Entity<User>().HasOne(e => e.Country);
            modelBuilder.Entity<Country>().HasKey(e => e.Id);
            modelBuilder.Entity<Country>().HasMany(e => e.Users);
            modelBuilder.Entity<Country>().HasMany(e => e.Breweries);

            modelBuilder.Entity<AlreadyDrunkBeers>().HasKey(e => new { e.BeerId, e.UserId });
            modelBuilder.Entity<BeerRating>().HasKey(e => new { e.BeerId, e.UserId });

            //modelBuilder.Entity<BeerReview>().HasKey(e => new { e.BeerId, e.UserId });
            //modelBuilder.Entity<BeerReview>().HasKey(e => new { e.BeerId, e.UserId });
            modelBuilder.Entity<BeerReview>().HasKey(x => x.Id);
            modelBuilder.Entity<BeerReview>().HasOne(x => x.User);
            modelBuilder.Entity<BeerReview>().HasOne(x => x.Beer);

            modelBuilder.Entity<WishListItem>().HasKey(e => new { e.BeerId, e.UserId });


            //modelBuilder.Entity<ReviewLike>()
            //   .HasKey(bc => new { bc.BeerReviewId, bc.UserId });
            //modelBuilder.Entity<ReviewLike>().HasOne(x => x.User);
            //modelBuilder.Entity<ReviewLike>().HasOne(x => x.BeerReview);

            //modelBuilder.Entity<ReviewLike>().HasKey(e => new { e.BeerReviewIdId, e.UserId });
            //modelBuilder.Entity<ReviewLike>()
            //    .HasOne(e => e.BeerReview)
            //    .WithMany(e => e.Likes)
            //    .HasPrincipalKey(e => e.Id)
            //    .HasForeignKey(e => e.Id);

            //     modelBuilder.Entity<ReviewLike>()
            //.HasKey(bc => new { bc.BeerReviewId, bc.UserId });
            //     modelBuilder.Entity<AlreadyDrunkBeers>()
            //         .HasOne(bc => bc.Beer)
            //         .WithMany(b => b.BeerReviews)
            //         .HasForeignKey(bc => bc.BeerReviewId);
            //     modelBuilder.Entity<AlreadyDrunkBeers>()
            //         .HasOne(bc => bc.User)
            //         .WithMany(c => c.BeerReviews)
            //         .HasForeignKey(bc => bc.UserId);

            modelBuilder.Entity<User>().HasMany(x => x.AlreadyDrunkBeers);

            modelBuilder.Entity<AlreadyDrunkBeers>()
                .HasKey(bc => new { bc.BeerId, bc.UserId });
            modelBuilder.Entity<AlreadyDrunkBeers>()
                .HasOne(bc => bc.Beer)
                .WithMany(b => b.AlreadyDrunkBeers)
                .HasForeignKey(bc => bc.BeerId);
            modelBuilder.Entity<AlreadyDrunkBeers>()
                .HasOne(bc => bc.User)
                .WithMany(c => c.AlreadyDrunkBeers)
                .HasForeignKey(bc => bc.UserId);

            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                            .SelectMany(t => t.GetForeignKeys())
                            .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
            {
                fk.DeleteBehavior = DeleteBehavior.Restrict;
            }

            //modelBuilder.Entity<BeerRating>().Key
            //modelBuilder.Entity<BeerRating>().HasNoKey();
            //modelBuilder.Entity<BeerReview>().HasNoKey();
            //modelBuilder.Entity<WishListItem>().HasNoKey();
            //modelBuilder.Entity<Side>()
            //    .HasRequired(s => s.Stage)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);
            //modelBuilder.Entity<Beer>()
            //    .Property(b => b.Name)
            //    .IsRequired();
            //modelBuilder.Entity<User>().HasKey(e => e.Id);
            //TODO: toggle seeder
            modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //Server = localhost\Valery'sMacbookPro; Database = BeerOverFlow; User Id = sa; Password = 1234567890;
            if (!optionsBuilder.IsConfigured)
            {
                PlatformID pid = Environment.OSVersion.Platform;
                switch (pid)
                {
                    case PlatformID.Unix:
                        optionsBuilder.UseSqlServer(@"Server=localhost\ValerySQL, 1433;Database=BeerOverFlowDB;User=sa;Password=Zackisawesome33;Trusted_Connection=False;");
                        break;
                    default:
                        optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=BeerOverflowDB;Trusted_Connection=True;");
                        break;
                }
            }
        }
    }
}
