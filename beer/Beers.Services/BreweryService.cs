﻿using Beers.Data;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Services.Extensions;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Services
{
    public class BreweryService : IBreweryService
    {
        private IDateTimeProvider datetimeProvider;
        private readonly BeersContext beersContext;

        public BreweryService(IDateTimeProvider datetimeProvider, BeersContext beersContext)
        {
            this.datetimeProvider = datetimeProvider;
            this.beersContext = beersContext;
        }

        public async Task<BreweryDTO> Create(BreweryDTO breweryDTO)
        {
            if (breweryDTO == null)
                throw new ArgumentNullException();

            var country = await this.beersContext.Countries
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Name.ToLower() == breweryDTO.Name.ToLower());
            //add country if not existing
            if (country == null)
                country = await CreateCountry(breweryDTO);

            var brewery = new Brewery()
            {
                Name = breweryDTO.Name,
                Country = country,
                CountryId = country.Id,
            };
            brewery.CreatedOn = datetimeProvider.GetDateTime();
            await this.beersContext.Breweries.AddAsync(brewery);
            await this.beersContext.SaveChangesAsync();

            return breweryDTO;
        }



        public async Task<bool> Delete(int id)
        {
            try
            {
                var brewery = await this.beersContext.Breweries
                    .Where(brewery => brewery.IsDeleted == false)
                    .FirstOrDefaultAsync(brewery => brewery.Id == id);

                brewery.IsDeleted = true;
                brewery.DeletedOn = this.datetimeProvider.GetDateTime();
                await this.beersContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        //TODO: check if returns correct count of beers
        public async Task<IEnumerable<BreweryDTO>> GetTopBreweries(int count = 4)
        {
            var result = await this.beersContext.Breweries
                .Where(x => x.IsDeleted == false)
                .Include(x => x.Country)
                    .ThenInclude(x => x.Breweries)
                .Include(x => x.Beers)
                    .ThenInclude(x => x.Style)
                .OrderByDescending(x => x.Beers.Count)
                .Take(count)
                .Select(x => x.ToBreweryDTO())
                //.AsNoTracking()
                .ToListAsync();

            return result;
        }

        public async Task<IEnumerable<BreweryDTO>> GetAllBreweries(int? page = 1, int? pagesize = 12, string filter = "", string order = "", string arrange = "", string country = "")
        {
            page = page ?? 1;
            pagesize = pagesize ?? 12;
            filter = filter ?? "";
            order = order ?? "name_asc";
            arrange = arrange ?? "asc";

            var result = await this.beersContext.Breweries
                    .AsNoTracking()
                    .Include(x => x.Country)
                    //.ThenInclude(x => x.Breweries)
                    .Include(x => x.Beers)
                        .ThenInclude(x => x.Style)
                    .Where(x => x.IsDeleted == false)
                    .Where(x => x.Name.ToLower().Contains(filter.ToLower()))
                    .FilterByCountry(country)//|| x.Country.Name.ToLower().Contains(filter.ToLower()) ))
                    .ToListAsync();
                    //.AsEnumerable();

            arrange = arrange.ToLower();

            //TODO: check these
            return order.ToLower() switch
            {
                "name_asc" => result.OrderBy(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(b => b.ToBreweryDTO()).ToList(),

                "name_desc" => result.OrderByDescending(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(b => b.ToBreweryDTO()).ToList(),

                "country" => result.OrderBy(x => x.Country.Name, arrange)
                    .ToPage((int)page, (int)pagesize)
                    .Select(b => b.ToBreweryDTO()).ToList(),

                _ => result.OrderBy(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(b => b.ToBreweryDTO()).ToList()
            };
        }

        public async Task<BreweryDTO> GetBrewery(int? id)
        {
            if (id == null)
                throw new ArgumentNullException();

            var brewery = await this.beersContext.Breweries
                    //.AsNoTracking()
                    .Where(brewery => brewery.IsDeleted == false)
                    .Include(x => x.Country)
                        .ThenInclude(x => x.Breweries)
                    .Include(x => x.Beers)
                        .ThenInclude(x => x.Style)
                    .FirstOrDefaultAsync(brewery => brewery.Id == id);

            if (brewery == null)
                return null;

            return brewery.ToBreweryDTO();
        }

        public async Task<BreweryDTO> Update(int id, BreweryDTO breweryDTO)
        {
            var UpdatedBrewery = await this.beersContext.Breweries
                .Include(x => x.Country)
                .Where(brewery => brewery.IsDeleted == false)
                .FirstOrDefaultAsync(brewery => brewery.Id == id);

            if (UpdatedBrewery == null)
                throw new ArgumentNullException();

            var country = await this.beersContext.Countries
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Name.ToLower() == breweryDTO.Name.ToLower());
            //add country if not existing
            if (country == null)
                country = await CreateCountry(breweryDTO);


            UpdatedBrewery.Name = breweryDTO.Name;
            UpdatedBrewery.CountryId = country.Id;
            UpdatedBrewery.Country = country;
            UpdatedBrewery.ModifiedOn = this.datetimeProvider.GetDateTime();
            await this.beersContext.SaveChangesAsync();
            return breweryDTO;
        }

        //Helper methods

        public async Task<int> BreweriesCount()
        {
            return await this.beersContext.Breweries.CountAsync();
        }


        public async Task<Country> CreateCountry(BreweryDTO breweryDTO)
        {
            Country country = new Country()
            {
                Name = breweryDTO.Country.Name,
                CreatedOn = DateTime.Now,
            };
            await this.beersContext.Countries.AddAsync(country);
            await this.beersContext.SaveChangesAsync();
            return country;
        }
        public async Task<bool> CountryExists(string countryName)
        {
            var country = await this.beersContext.Countries
                .Where(c => c.IsDeleted == false)
                .FirstOrDefaultAsync(c => c.Name.ToLower() == countryName.ToLower());

            return country != null ? true : false;
        }

        public async Task<bool> BreweryExists(string breweryName, string breweryCountry)
        {
            var brewery = await this.beersContext.Breweries
                .Include(x => x.Country)
                .Where(brewery => brewery.IsDeleted == false)
                .FirstOrDefaultAsync(brewery => brewery.Name.ToLower() == breweryName.ToLower()
                                     && brewery.Country.Name.ToLower() == breweryCountry.ToLower());

            return brewery != null ? true : false;
        }

    }
}
