﻿using Beers.Models;
using Beers.Services.DTOs;
using System.Linq;

namespace Beers.Services
{
    public static class DTOFetcher
    {
        /// <summary>
        /// Accepts instance of BeerDTO and refactor it to Beer
        /// </summary>
        /// <param name="beerDTO">instance of BeerDTO</param>
        /// <returns>instance of Beer</returns>
        //public static Beer ToBeer(this BeerDTO beerDTO)
        //{
        //    return new Beer
        //    {
        //        Id = beerDTO.Id,
        //        Name = beerDTO.Name,
        //        Description = beerDTO.Description,
        //        StyleId = beerDTO.StyleId,
        //        BreweryId = beerDTO.BreweryId,
        //        ABV = beerDTO.ABV,
        //    };
        //}

        /// <summary>
        /// Accepts instance of Beer and refactor it to DTO
        /// </summary>
        /// <param name="beer">instance of Beer</param>
        /// <returns>instance of BeerDTO</returns>
        public static BeerDTO ToBeerDTO(this Beer beer)//, BeersContext beersContext)

        {
            //var style = beersContext.Styles.FirstOrDefault(s => s.Id == beer.StyleId).Name;
            return new BeerDTO()//beersContext
            {
                Id = beer.Id,
                Name = beer.Name,
                Description = beer.Description,
                ABV = beer.ABV,
                ImageURL = beer.ImageURL,
                //TODO: See this (returns null reference)
                Style = beer.Style.Name,
                Brewery = beer.Brewery.Name,

                AverageRating = beer.BeerReviews.Count() > 0 ? beer.BeerReviews.Average(x => x.Rating) : 0,
                //AverageRating = beer.BeerRatings.Count > 0 ? beer.BeerRatings.Average(x => x.Rating) : 0,
                Reviews = beer.BeerReviews.ToList(),
            };
        }

        /// <summary>
        /// Accepts instance of BreweryDTO and refactor it to Brewery
        /// </summary>
        /// <param name="breweryDTO">instance of BreweryDTO</param>
        /// <returns>instance of Brewery</returns>
        //public static Brewery ToBrewery(this BreweryDTO breweryDTO)
        //{
        //    return new Brewery
        //    {
        //        Id = breweryDTO.Id,
        //        Name = breweryDTO.Name,
        //        //CountryId = breweryDTO.CountryId
        //    };
        //}

        /// <summary>
        /// Accepts instance of Brewery and refactor it to DTO
        /// </summary>
        /// <param name="brewery">instance of Brewery</param>
        /// <returns>instance of BreweryDTO</returns>
        public static BreweryDTO ToBreweryDTO(this Brewery brewery)//, BeersContext beersContext)
        {
            var brew = new BreweryDTO();
            brew.Id = brewery.Id;
            brew.Name = brewery.Name;
            if (brewery.Country != null)
            {
                brew.Country = new CountryDTO(brewery.Country.Id, brewery.Country.Name);//brewery.Country.ToCountryDTO();
            }
            brew.Beers = brewery.Beers
                .Where(x => x.IsDeleted == false)
                .Select(x => x.ToBeerDTO()).ToList();
            //return new BreweryDTO()//beersContext
            //{
            //    Id = brewery.Id,
            //    Name = brewery.Name,
            //    Country = brewery.Country.ToCountryDTO(),
            //    Beers = brewery.Beers.Select(x => x.ToBeerDTO()).ToList()
            //};
            return brew;
        }
        /// <summary>
        /// Accepts instance of CountryDTO and refactor it to Country
        /// </summary>
        /// <param name="countryDTO">instance of CountryDTO</param>
        /// <returns>instance of Country</returns>
        public static Country ToCountry(this CountryDTO countryDTO)
        {
            return new Country
            {
                Id = countryDTO.Id,
                Name = countryDTO.Name,
            };
        }

        /// <summary>
        /// Accepts instance of Country and refactor it to DTO
        /// </summary>
        /// <param name="country">instance of Country</param>
        /// <returns>instance of CountryDTO</returns>
        public static CountryDTO ToCountryDTO(this Country country)
        {
            return new CountryDTO
            {
                Id = country.Id,
                Name = country.Name,
                Breweries = country.Breweries
                    .Where(x => x.IsDeleted == false)
                    .Select(x => x.ToBreweryDTO()).ToList(),
            };
        }

        /// <summary>
        /// Accepts instance of StyleDTO and refactor it to Style
        /// </summary>
        /// <param name="userDTO">instance of StyleDTO</param>
        /// <returns>instance of Style</returns>
        //public static Style ToStyle(this StyleDTO styleDTO)
        //{
        //    return new Style
        //    {
        //        Id = styleDTO.Id,
        //        Name = styleDTO.Name,
        //    };
        //}

        /// <summary>
        /// Accepts instance of Style and refactor it to DTO
        /// </summary>
        /// <param name="style">instance of Style</param>
        /// <returns>instance of StyleDTO</returns>
        public static StyleDTO ToStyleDTO(this Style style)
        {
            return new StyleDTO
            {
                Id = style.Id,
                Name = style.Name,
                Beers = style.Beers
                    .Where(x => x.IsDeleted == false)
                    .Select(x => x.ToBeerDTO()).ToList(),
            };
        }

        /// <summary>
        /// Accepts instance of UserDTO and refactor it to User
        /// </summary>
        /// <param name="userDTO">instance of UserDTO</param>
        /// <returns>instance of User</returns>
        //public static User ToUser(this UserDTO userDTO)
        //{
        //    return new User
        //    {
        //        Id = userDTO.Id,
        //        Username = userDTO.Username,
        //        AlreadyDrunkBeers = userDTO.AlreadyDrunkBeers,
        //        Password = userDTO.Password,
        //        Email = userDTO.Email,
        //        CountryId = userDTO.CountryId,
        //        Age = userDTO.Age,
        //        ImageURL = userDTO.ImageURL,
        //        IsBanned = userDTO.IsBanned,
        //        IsAdmin = userDTO.IsAdmin
        //    };
        //}

        /// <summary>
        /// Accepts instance of User and refactor it to DTO
        /// </summary>
        /// <param name="user">instance of User</param>
        /// <returns>instance of UserDTO</returns>
        public static UserDTO ToUserDTO(this User user)
        {
            return new UserDTO
            {
                Id = user.Id,
                UserName = user.UserName,
                //Password = user.PasswordHash,
                Email = user.Email,
                //CountryId = user.CountryId,
                Age = user.Age,
                ImageURL = user.ImageURL,
                IsBanned = user.IsBanned,
                IsAdmin = user.IsAdmin
            };
        }
    }
}
