﻿using Beers.Models;
using System.Collections.Generic;

namespace Beers.Services.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
        public ICollection<WishListItem> WishList { get; set; }
        public ICollection<AlreadyDrunkBeers> AlreadyDrunkBeers { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int CountryId { get; set; }
        public string Country { get; set; }
        public int Age { get; set; }
        public string ImageURL { get; set; }
        public bool IsBanned { get; set; }
        public bool IsAdmin { get; set; }

        public string UserName { get; set; }
    }
}
