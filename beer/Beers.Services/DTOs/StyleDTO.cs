﻿using System.Collections.Generic;

namespace Beers.Services.DTOs
{
    public class StyleDTO
    {
        public StyleDTO()
        {
        }
        public StyleDTO(string name)
        {
            this.Name = name;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public List<BeerDTO> Beers { get; set; } = new List<BeerDTO>();
    }
}
