﻿using System.Collections.Generic;

namespace Beers.Services.DTOs
{
    public class CountryDTO
    {
        public CountryDTO()
        {
        }
        public CountryDTO(string name)
        {
            this.Name = name;
        }

        public CountryDTO(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<BreweryDTO> Breweries { get; set; } = new List<BreweryDTO>();
    }
}
