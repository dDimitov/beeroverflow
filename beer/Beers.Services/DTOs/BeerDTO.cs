﻿using Beers.Models;
using System.Collections.Generic;

namespace Beers.Services.DTOs
{
    public class BeerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double ABV { get; set; }
        public double AverageRating { get; set; }
        public string ImageURL { get; set; }

        public string Style { get; set; }
        public string Brewery { get; set; }

        public List<BeerReview> Reviews { get; set; }
    }
}
