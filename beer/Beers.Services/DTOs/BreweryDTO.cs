﻿using System.Collections.Generic;

namespace Beers.Services.DTOs
{
    public class BreweryDTO
    {
        public BreweryDTO()
        {
        }
        public BreweryDTO(string name, string country)
        {
            this.Name = name;
            this.Country = new CountryDTO(country);
        }
        public int Id { get; set; }
        public string Name { get; set; }
        //public string Country { get; set; }
        public CountryDTO Country { get; set; }
        public ICollection<BeerDTO> Beers { get; set; }

    }
}
