﻿using Beers.Data;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Services.Extensions;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Services
{
    public class CountryService : ICountryService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeersContext beersContext;

        public CountryService(IDateTimeProvider dateTimeProvider, BeersContext beersContext)
        {
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this.beersContext = beersContext;
        }
        public async Task<CountryDTO> Create(CountryDTO countryDTO)
        {
            if (countryDTO == null)
                throw new ArgumentNullException();

            Country country = new Country() { Name = countryDTO.Name };
            country.CreatedOn = this.dateTimeProvider.GetDateTime();

            await this.beersContext.Countries.AddAsync(country);
            await this.beersContext.SaveChangesAsync();

            return countryDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var country = await this.beersContext.Countries
                    .Where(country => country.IsDeleted == false)
                    .FirstOrDefaultAsync(country => country.Id == id);

                country.IsDeleted = true;
                country.DeletedOn = this.dateTimeProvider.GetDateTime();
                await this.beersContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IEnumerable<CountryDTO>> GetAllCountries(int? page = 1, int? pagesize = 10, string filter = "", string order = "", string arrange = "")
        {
            page = page ?? 1;
            pagesize = pagesize ?? 10;
            filter = filter ?? "";
            order = order ?? "name";
            arrange = arrange ?? "asc";

            var result = this.beersContext.Countries
                .Include(x => x.Breweries)
                        .ThenInclude(x => x.Beers)
                            .ThenInclude(x => x.Style)
                    .Where(x => x.IsDeleted == false && x.Name.ToLower().Contains(filter.ToLower()))
                    .AsEnumerable();

            arrange = arrange.ToLower();

            return order.ToLower() switch
            {
                "name_asc" => result.OrderBy(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(c => c.ToCountryDTO()).ToList(),

                "name_desc" => result.OrderByDescending(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(c => c.ToCountryDTO()).ToList(),

                _ => result.OrderBy(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(c => c.ToCountryDTO()).ToList()
            };
        }

        public async Task<CountryDTO> GetCountry(int? id)
        {
            if (id == null)
                throw new ArgumentNullException();

            var country = await this.beersContext.Countries
                .Include(x => x.Breweries)
                        .ThenInclude(x => x.Beers)
                            .ThenInclude(x => x.Style)
                .Where(country => country.IsDeleted == false)
                .AsNoTracking()
                .FirstOrDefaultAsync(country => country.Id == id);

            if (country == null)
                throw new ArgumentNullException();

            return country.ToCountryDTO();
        }

        public async Task<CountryDTO> GetCountry(string name)
        {
            if (name == null)
                throw new ArgumentNullException();

            var country = await this.beersContext.Countries
                .Include(x => x.Breweries)
                        .ThenInclude(x => x.Beers)
                            .ThenInclude(x => x.Style)
                .Where(country => country.IsDeleted == false)
                .AsNoTracking()
                .FirstOrDefaultAsync(country => country.Name.ToLower() == name.ToLower());

            if (country == null)
                throw new ArgumentNullException();

            return country.ToCountryDTO();
        }

        public async Task<CountryDTO> Update(int id, CountryDTO countryDTO)
        {
            var countryToUpdate = await this.beersContext.Countries
                .Where(country => country.IsDeleted == false)
                .FirstOrDefaultAsync(country => country.Id == id);

            if (countryToUpdate == null)
                throw new ArgumentNullException();

            //countryToUpdate.Id = countryDTO.Id;
            countryToUpdate.Name = countryDTO.Name;
            countryToUpdate.ModifiedOn = this.dateTimeProvider.GetDateTime();
            await this.beersContext.SaveChangesAsync();

            return countryDTO;
        }

        //Helper methods
        public async Task<bool> CountryExists(string countryName)
        {
            var country = await this.beersContext.Countries
                .Where(country => country.IsDeleted == false)
                .AsNoTracking()
                .FirstOrDefaultAsync(country => country.Name.ToLower() == countryName.ToLower());

            return country != null ? true : false;
        }
    }
}
