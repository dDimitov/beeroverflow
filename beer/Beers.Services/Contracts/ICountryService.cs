﻿using Beers.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Beers.Services.Contracts
{
    public interface ICountryService
    {
        Task<CountryDTO> GetCountry(int? id);
        Task<CountryDTO> GetCountry(string name);
        Task<IEnumerable<CountryDTO>> GetAllCountries(int? page = 1, int? pagesize = 10, string filter = "", string order = "", string arrange = "");
        Task<CountryDTO> Create(CountryDTO countryDTO);
        Task<CountryDTO> Update(int id, CountryDTO countryDTO);
        public Task<bool> Delete(int id);
        Task<bool> CountryExists(string countryName);
    }
}
