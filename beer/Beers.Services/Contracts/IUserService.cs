﻿using Beers.Models;
using Beers.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Beers.Services.Contracts
{
    public interface IUserService
    {
        Task<UserDTO> GetUser(int? id);
        Task<IEnumerable<UserDTO>> GetAllUsers(int? page = 1, int? pagesize = 10, string filter = "");
        Task<UserDTO> Create(UserDTO userDTO);
        Task<UserDTO> Update(int id, UserDTO userDTO);
        public Task<bool> Delete(int id);
        Task<IEnumerable<BeerDTO>> GetAlreadyDrunkList(User user);
        Task<IEnumerable<BeerDTO>> GetWishList(User user);
        Task<bool> UserExists(string name);
    }
}
