﻿using Beers.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Beers.Services.Contracts
{
    public interface IStyleService
    {
        Task<StyleDTO> GetStyle(int? id);
        Task<IEnumerable<StyleDTO>> GetTopStyles(int count = 4);
        Task<IEnumerable<StyleDTO>> GetAllStyles(int? page = 1, int? pagesize = 10, string filter = "", string order = "", string arrange = "");
        Task<StyleDTO> Create(StyleDTO styleDTO);
        Task<StyleDTO> Update(int id, StyleDTO styleDTO);
        public Task<bool> Delete(int id);

        Task<int> StylesCount();
        Task<bool> StyleExists(string styleName);

    }
}
