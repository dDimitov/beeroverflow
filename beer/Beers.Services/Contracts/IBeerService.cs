﻿using Beers.Models;
using Beers.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Beers.Services.Contracts
{
    public interface IBeerService
    {
        Task<BeerDTO> GetBeer(int? id);
        Task<IEnumerable<BeerDTO>> GetTopBeers(int count = 3);
        Task<IEnumerable<BeerDTO>> GetAllBeers(int? page = 1, int? pagesize = 10, string filter = "", string order = "", string arrange = "", string country = "", string style = "");
        //IEnumerable<BeerDTO> GetAllBeers(int? page = 1, int? pagesize = 10, string filter = "", string order = "", string arrange = "", string country = "", string style = "");
        Task<BeerDTO> Create(BeerDTO beerDTO);
        Task<BeerDTO> Update(int id, BeerDTO beerDTO);
        public Task<bool> Delete(int id);


        Task AddBeerToAlreadyDrunkList(User user, int beerId);
        Task RemoveBeerFromAlreadyDrunkList(User user, int beerId);
        Task AddToWishList(User user, int beerId);
        Task RemoveBeerFromWishList(User user, int beerId);

        Task AddReview(User user, int beerId, int rating, string text);


        Task<int> BeersCount();
        Task<bool> BeerExists(string beerName);
    }
}
