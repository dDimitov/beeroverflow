﻿using Beers.Models;
using Beers.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Beers.Services.Contracts
{
    public interface IBreweryService
    {
        Task<BreweryDTO> GetBrewery(int? id);
        Task<IEnumerable<BreweryDTO>> GetTopBreweries(int count = 4);
        Task<IEnumerable<BreweryDTO>> GetAllBreweries(int? page = 1, int? pagesize = 10, string filter = "", string order = "", string arrange = "", string country = "");
        Task<BreweryDTO> Create(BreweryDTO breweryDTO);
        Task<BreweryDTO> Update(int id, BreweryDTO breweryDTO);
        public Task<bool> Delete(int id);

        Task<int> BreweriesCount();
        Task<bool> BreweryExists(string breweryName, string breweryCountry);
        Task<Country> CreateCountry(BreweryDTO breweryDTO);
        Task<bool> CountryExists(string countryName);
    }
}
