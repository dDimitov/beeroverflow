﻿using Beers.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Beers.Services.Extensions
{
    public static class Extensions
    {
        public static IOrderedEnumerable<TDTO> OrderBy<TDTO, T>(this IEnumerable<TDTO> query, Func<TDTO, T> keySelector, string order)
        {
            return order.ToLower() switch
            {
                "asc" => query.OrderBy(keySelector),
                "desc" => query.OrderByDescending(keySelector),
                _ => query.OrderBy(keySelector)
            };
        }

        public static IOrderedEnumerable<TDTO> OrderBy<TDTO, T>(this ICollection<TDTO> query, Func<TDTO, T> keySelector, string order)
        {
            return order.ToLower() switch
            {
                "asc" => query.OrderBy(keySelector),
                "desc" => query.OrderByDescending(keySelector),
                _ => query.OrderBy(keySelector)
            };
        }

        public static IEnumerable<T> ToPage<T>(this IEnumerable<T> query, int page, int pagesize)
        {
            return query
                .Skip(((int)page - 1) * (int)pagesize)
                .Take((int)pagesize);
        }

        public static IQueryable<Beer> FilterByCountry(this IQueryable<Beer> query, string country)
        {
            if (!string.IsNullOrEmpty(country))
                return query.Where(x => x.Brewery.Country.Name.ToLower().Contains(country.ToLower()));

            return query;
        }

        public static IQueryable<Brewery> FilterByCountry(this IQueryable<Brewery> query, string country)
        {
            if (!string.IsNullOrEmpty(country))
                return query.Where(x => x.Country.Name.ToLower().Contains(country.ToLower()));

            return query;
        }

        public static IQueryable<Beer> FilterByStyle(this IQueryable<Beer> query, string style)
        {
            if (!string.IsNullOrEmpty(style))
                return query.Where(x => x.Style.Name.ToLower().Contains(style.ToLower()));

            return query;
        }
    }
}
