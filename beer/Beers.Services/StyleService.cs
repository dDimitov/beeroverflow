﻿using Beers.Data;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Services.Extensions;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Services
{
    public class StyleService : IStyleService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeersContext beersContext;

        public StyleService(IDateTimeProvider dateTimeProvider, BeersContext beersContext)
        {
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this.beersContext = beersContext;
        }

        public async Task<StyleDTO> Create(StyleDTO styleDTO)
        {
            if (styleDTO == null)
                throw new ArgumentNullException();

            Style style = new Style() { Name = styleDTO.Name };//.ToStyle();
            style.CreatedOn = this.dateTimeProvider.GetDateTime();
            await this.beersContext.Styles.AddAsync(style);
            await this.beersContext.SaveChangesAsync();
            return styleDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var style = await this.beersContext.Styles
                    .Where(style => style.IsDeleted == false)
                    .FirstOrDefaultAsync(style => style.Id == id);

                style.IsDeleted = true;
                style.DeletedOn = this.dateTimeProvider.GetDateTime();
                await this.beersContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IEnumerable<StyleDTO>> GetTopStyles(int count = 4)
        {
            var result = await this.beersContext.Styles
                .Include(x => x.Beers)
                    .ThenInclude(x => x.Brewery)
                .Where(x => x.IsDeleted == false)
                .OrderByDescending(x => x.Beers.Count)
                .Take(count)
                .Select(x => x.ToStyleDTO())
                .AsNoTracking()
                .ToListAsync();

            return result;
        }

        public async Task<IEnumerable<StyleDTO>> GetAllStyles(int? page = 1, int? pagesize = 10, string filter = "", string order = "", string arrange = "")
        {
            page = page ?? 1;
            pagesize = pagesize ?? 10;
            filter = filter ?? "";
            order = order ?? "name";
            arrange = arrange ?? "asc";

            var result = this.beersContext.Styles
                .Include(x => x.Beers)
                    .ThenInclude(x => x.Brewery)
                .Where(style => style.IsDeleted == false && style.Name.ToLower().Contains(filter.ToLower()))
                .AsEnumerable();

            arrange = arrange.ToLower();

            return order.ToLower() switch
            {
                "name_asc" => result.OrderBy(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(c => c.ToStyleDTO()).ToList(),

                "name_desc" => result.OrderByDescending(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(c => c.ToStyleDTO()).ToList(),

                _ => result.OrderBy(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(c => c.ToStyleDTO()).ToList()
            };
        }

        public async Task<StyleDTO> GetStyle(int? id)
        {
            if (id == null)
                throw new ArgumentNullException();

            var style = await this.beersContext.Styles
                .Include(x => x.Beers)
                    .ThenInclude(x => x.Brewery)
                .Where(style => style.IsDeleted == false)
                .FirstOrDefaultAsync(style => style.Id == id);

            if (style == null)
                return null;

            return style.ToStyleDTO();
        }

        public async Task<StyleDTO> Update(int id, StyleDTO styleDTO)
        {
            var styleToUpdate = await this.beersContext.Styles
                .Include(x => x.Beers)
                    .ThenInclude(x => x.Brewery)
                .Where(style => style.IsDeleted == false)
                .FirstOrDefaultAsync(style => style.Id == id);

            if (styleToUpdate == null)
                throw new ArgumentNullException();

            styleToUpdate.Name = styleDTO.Name;
            styleToUpdate.ModifiedOn = this.dateTimeProvider.GetDateTime();
            await this.beersContext.SaveChangesAsync();
            return styleDTO;
        }

        //Helper methods

        public async Task<int> StylesCount()
        {
            return await this.beersContext.Styles.CountAsync();
        } 

        public async Task<bool> StyleExists(string styleName)
        {
            var style = await this.beersContext.Styles
                .Where(style => style.IsDeleted == false)
                .FirstOrDefaultAsync(style => style.Name.ToLower() == styleName.ToLower());

            return style != null ? true : false;
        }
    }
}
