﻿using Beers.Data;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Services.Extensions;
using Beers.Services.Providers.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Services
{
    public class UserService : IUserService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeersContext beersContext;
        private readonly UserManager<User> userManager;

        //private readonly User user;

        public UserService(IDateTimeProvider dateTimeProvider, BeersContext beersContext, UserManager<User> userManager)
        {
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this.beersContext = beersContext;
            this.userManager = userManager;
        }

        public async Task<UserDTO> Create(UserDTO userDTO)
        {
            if (userDTO == null)
                throw new ArgumentNullException();

            var country = await this.beersContext.Countries
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Name.ToLower() == userDTO.Country.ToLower());

            //add country if not existing
            if (country == null)
                country = await CreateCountry(userDTO);

            User user = new User()
            {
                Name = userDTO.Name,
                //Password = userDTO.Password,
                Email = userDTO.Email,
                Age = userDTO.Age,
                ImageURL = userDTO.ImageURL,

                //CountryId = country.Id,
                //Country = country,

                CreatedOn = this.dateTimeProvider.GetDateTime()
            };

            await this.beersContext.Users.AddAsync(user);
            await this.beersContext.SaveChangesAsync();

            return userDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var user = await this.beersContext.Users
                    .Where(user => user.IsDeleted == false)
                    .FirstOrDefaultAsync(user => user.Id == id);

                user.IsDeleted = true;
                user.DeletedOn = this.dateTimeProvider.GetDateTime();

                await this.beersContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IEnumerable<UserDTO>> GetAllUsers(int? page = 1, int? pagesize = 10, string filter = "")
        {
            page = page ?? 1;
            pagesize = pagesize ?? 10;
            filter = filter ?? "";

            var users = this.userManager.Users
                .Where(x => x.IsDeleted == false)
                .ToPage((int)page, (int)pagesize)
                .Select(x => x.ToUserDTO())
                .ToList();

            return users;
            //return await this.beersContext.Users
            //    //.Include(x => x.Country)
            //    .Include(x => x.Ratings)
            //    .Include(x => x.Reviews)
            //    .Include(x => x.WishList)
            //    .Include(x => x.AlreadyDrunkBeers)
            //    .Where(user => user.IsDeleted == false && user.UserName.ToLower().Contains(filter.ToLower()))
            //    .Skip(((int)page - 1) * (int)pagesize)
            //    .Take((int)pagesize)
            //    .Select(user => user.ToUserDTO())
            //    .ToListAsync();
        }

        public async Task<UserDTO> GetUser(int? id)
        {
            if (id == null)
                throw new ArgumentNullException();

            var user = await this.beersContext.Users
                //.Include(x => x.Country)
                .Include(x => x.Ratings)
                .Include(x => x.Reviews)
                .Include(x => x.WishList)
                .Include(x => x.AlreadyDrunkBeers)
                .Where(user => user.IsDeleted == false)
                .FirstOrDefaultAsync(user => user.Id == id);

            if (user == null)
                throw new ArgumentNullException();

            var userDTO = user.ToUserDTO();

            return userDTO;
        }

        public async Task<UserDTO> Update(int id, UserDTO userDTO)
        {
            User userToUpdate = await this.beersContext.Users
                //.Include(x => x.Country)
                .Include(x => x.Ratings)
                .Include(x => x.Reviews)
                .Include(x => x.WishList)
                .Include(x => x.AlreadyDrunkBeers)
                .Where(user => user.IsDeleted == false)
                .FirstOrDefaultAsync(user => user.Id == id);

            if (userToUpdate == null)
                throw new ArgumentNullException();

            var country = await this.beersContext.Countries
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Name.ToLower() == userDTO.Country.ToLower());

            //add country if not existing
            if (country == null)
                country = await CreateCountry(userDTO);

            userToUpdate.Name = userDTO.Name;
            //userToUpdate.Password = userDTO.Password;
            userToUpdate.Email = userDTO.Email;
            userToUpdate.Age = userDTO.Age;
            userToUpdate.ImageURL = userDTO.ImageURL;

            userToUpdate.CountryId = country.Id;
            //userToUpdate.Country = country;

            userToUpdate.ModifiedOn = this.dateTimeProvider.GetDateTime();
            //userToUpdate.AlreadyDrunkBeers = userDTO.AlreadyDrunkBeers;

            //TODO: Edit update information
            //userToUpdate.IsBanned = userDTO.IsBanned;
            //userToUpdate.IsAdmin = userDTO.IsAdmin;

            await this.beersContext.SaveChangesAsync();

            return userDTO;
        }



        public async Task<IEnumerable<BeerDTO>> GetAlreadyDrunkList(User user)
        {
            //var user = await this.beersContext.Users
            //    .Include(x => x.AlreadyDrunkBeers)
            //    .Include(a => a.AlreadyDrunkBeers.Select(x => x.Beer))
            //        .ThenInclude(b => b.Style)
            //    .Include(a => a.AlreadyDrunkBeers.Select(x => x.Beer.Brewery))
            //    .Where(x => x.IsDeleted == false)
            //    .FirstOrDefaultAsync(x => x.Id == userId);

            if (user == null)
                throw new ArgumentNullException();

            //return user.AlreadyDrunkBeers
            //    .Select(x => x.Beer.ToBeerDTO())
            //    .ToList();
            var result = await this.beersContext.AlreadyDrunkBeers
                .Where(x => x.IsDeleted == false && x.Beer.IsDeleted == false && x.User.IsDeleted == false)
                .Where(x => x.UserId == user.Id)
                .Include(x => x.Beer)
                .Include(x => x.Beer.Style)
                .Include(x => x.Beer.Brewery)
                .Include(x => x.Beer.BeerReviews)
                .Select(x => x.Beer.ToBeerDTO())
                .ToListAsync();

            return result;
        }

        public async Task<IEnumerable<BeerDTO>> GetWishList(User user)
        {
            //var user = await this.beersContext.Users
            //    .Include(x => x.WishList)
            //    .Include(a => a.WishList.Select(x => x.Beer))
            //        .ThenInclude(b => b.Style)
            //    .Include(a => a.WishList.Select(x => x.Beer.Brewery))
            //    .Where(x => x.IsDeleted == false)
            //    .FirstOrDefaultAsync(x => x.Id == userId);

            if (user == null)
                throw new ArgumentNullException();

            //return user.WishList
            //    .Select(x => x.Beer.ToBeerDTO())
            //    .ToList();
            var result = await this.beersContext.WishLists
                .Where(x => x.IsDeleted == false)
                .Where(x => x.UserId == user.Id)
                .Include(x => x.Beer)
                .Include(x => x.Beer.Style)
                .Include(x => x.Beer.Brewery)
                .Include(x => x.Beer.BeerReviews)
                .Select(x => x.Beer.ToBeerDTO())
                .ToListAsync();
            return result;
        }







        //Helper methods------------------------------------------------------



        private async Task<Country> CreateCountry(UserDTO userDTO)
        {
            Country country = new Country()
            {
                Name = userDTO.Country,
                CreatedOn = DateTime.Now,
            };
            await this.beersContext.Countries.AddAsync(country);
            await this.beersContext.SaveChangesAsync();
            return country;
        }
        public async Task<bool> UserExists(string name)
        {
            var user = await this.beersContext.Users
                .Where(user => user.IsDeleted == false)
                .FirstOrDefaultAsync(user => user.UserName.ToLower() == name.ToLower());

            return user != null ? true : false;
        }
    }
}
