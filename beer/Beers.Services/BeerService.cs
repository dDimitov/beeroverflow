﻿using Beers.Data;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Services.Extensions;
using Beers.Services.Providers.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Services
{
    public class BeerService : IBeerService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeersContext beersContext;
        private readonly UserManager<User> _userManager;

        public BeerService(IDateTimeProvider dateTimeProvider, BeersContext beersContext, UserManager<User> userManager)
        {
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this.beersContext = beersContext;
            _userManager = userManager;
        }


        public async Task AddBeerToAlreadyDrunkList(User user, int beerId)
        {

            if (user == null)
                throw new ArgumentNullException("Cannot add a beer to already drunk list if user is null of deleted");

            var beerToAdd = await beersContext.Beers
                .Include(x => x.AlreadyDrunkBeers)
                .Include(x => x.BeerRatings)
                .Include(x => x.BeerReviews)
                //.ThenInclude(x => x.Likes)
                .Include(x => x.Brewery)
                .Include(x => x.Style)
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(beer => beer.Id == beerId);

            if (this.beersContext.AlreadyDrunkBeers.Any(x => x.BeerId == beerToAdd.Id && x.IsDeleted == true))
            {
                var item = await this.beersContext.AlreadyDrunkBeers.FirstOrDefaultAsync(x => x.BeerId == beerToAdd.Id);
                item.ModifiedOn = DateTime.Now;
                item.IsDeleted = false;
                await this.beersContext.SaveChangesAsync();
                return;
            }

            var alreadydrunk = new AlreadyDrunkBeers()
            {
                User = user,
                UserId = user.Id,
                Beer = beerToAdd,
                BeerId = beerToAdd.Id
            };

            alreadydrunk.CreatedOn = DateTime.Now;

            if (beerToAdd.AlreadyDrunkBeers == null)
                beerToAdd.AlreadyDrunkBeers = new List<AlreadyDrunkBeers>();

            if (user.AlreadyDrunkBeers == null)
                user.AlreadyDrunkBeers = new List<AlreadyDrunkBeers>();



            //beerToAdd.AlreadyDrunkBeers.Add(alreadydrunk);
            //user.AlreadyDrunkBeers.Add(alreadydrunk);
            await this.beersContext.AlreadyDrunkBeers.AddAsync(alreadydrunk);
            await this.beersContext.SaveChangesAsync();
            //await _userManager.UpdateAsync(user);
        }

        public async Task RemoveBeerFromAlreadyDrunkList(User user, int beerId)
        {
            if (user == null)
                throw new ArgumentNullException();

            //user.AlreadyDrunkBeers.Remove(beer);
            //await _userManager.UpdateAsync(user);
            var drunkItem = this.beersContext.AlreadyDrunkBeers.FirstOrDefault(x => x.BeerId == beerId && x.UserId == user.Id);

            drunkItem.DeletedOn = DateTime.Now;
            drunkItem.IsDeleted = true;
            await this.beersContext.SaveChangesAsync();
        }


        public async Task AddToWishList(User user, int beerId)
        {
            if (user == null)
                throw new ArgumentNullException();

            var beer = await this.beersContext.Beers
                .Include(x => x.AlreadyDrunkBeers)
                .Include(x => x.BeerRatings)
                .Include(x => x.BeerReviews)
                //.ThenInclude(x => x.Likes)
                .Include(x => x.Brewery)
                .Include(x => x.Style)
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(beer => beer.Id == beerId);

            if (beer == null)
                throw new ArgumentNullException();

            if (this.beersContext.WishLists.Any(x => x.BeerId == beer.Id && x.IsDeleted == true))
            {
                var item = await this.beersContext.WishLists.FirstOrDefaultAsync(x => x.BeerId == beer.Id);
                item.ModifiedOn = DateTime.Now;
                item.IsDeleted = false;
                await this.beersContext.SaveChangesAsync();
                return;
            }

            var WishListItem = new WishListItem
            {
                UserId = user.Id,
                User = user,
                BeerId = beer.Id,
                Beer = beer
            };

            WishListItem.CreatedOn = DateTime.Now;

            user.WishList.Add(WishListItem);

            this.beersContext.Users.FirstOrDefault(x => x.Id == user.Id).WishList.Add(WishListItem);
            await this.beersContext.WishLists.AddAsync(WishListItem);
            await _userManager.UpdateAsync(user);
            await this.beersContext.SaveChangesAsync();
            //await this.beersContext.SaveChangesAsync();
        }


        public async Task RemoveBeerFromWishList(User user, int beerId)
        {
            if (user == null)
                throw new ArgumentNullException();

            var wishlistItem = this.beersContext.WishLists.FirstOrDefault(x => x.BeerId == beerId && x.UserId == user.Id);

            if (wishlistItem == null)
                throw new ArgumentNullException();

            //user.WishList.Remove(wishlistItem);
            //await _userManager.UpdateAsync(user);
            wishlistItem.IsDeleted = true;
            wishlistItem.DeletedOn = DateTime.Now;
            await this.beersContext.SaveChangesAsync();
        }


        public async Task<BeerDTO> GetBeer(int? id)
        {
            if (id == null)
                throw new ArgumentNullException();

            var beer = await this.beersContext.Beers
                .Include(s => s.Style)
                .Include(b => b.Brewery)
                .Include(x => x.BeerRatings)
                .Include(x => x.BeerReviews)
                    .ThenInclude(x => x.User)
                .Where(beer => beer.IsDeleted == false)
                .FirstOrDefaultAsync(beer => beer.Id == id);

            if (beer == null)
                throw new ArgumentNullException();

            return beer.ToBeerDTO();
        }

        public async Task<IEnumerable<BeerDTO>> GetTopBeers(int count = 3)
        {
            if (count == 0) count = 3;

            var result = await this.beersContext.Beers
            .Include(s => s.Style)
            .Include(b => b.Brewery)
            .Include(x => x.BeerRatings)
            .Include(x => x.BeerReviews)
            .Where(beer => beer.IsDeleted == false)
            .Take(count)
            .Select(b => b.ToBeerDTO())
            .ToListAsync();

            return result;
        }

        //TODO: try to make it filter by searching in all beer properties // (optional) include style and country // Add paging here !!!
        public async Task<IEnumerable<BeerDTO>> GetAllBeers(int? page = 1, int? pagesize = 10, string filter = "", string order = "", string arrange = "", string country = "", string style = "")
        {

            page = page ?? 1;
            pagesize = pagesize ?? 12;
            filter = filter ?? "";
            order = order ?? "name";
            arrange = arrange ?? "asc";

            var result = await this.beersContext.Beers
            .Include(s => s.Style)
            .Include(b => b.Brewery)
            .Include(x => x.BeerReviews)
            .Where(beer => beer.IsDeleted == false)
            .Where(beer => beer.Name.ToLower().Contains(filter.ToLower()))
            .FilterByCountry(country)
            .FilterByStyle(style)
            .ToListAsync();
            //|| beer.ABV.ToString().Contains(filter)
            //|| beer.Brewery.Name.ToLower().Contains(filter.ToLower())
            //|| beer.Style.Name.ToLower().Contains(filter.ToLower()))
            //.AsEnumerable();

            arrange = arrange.ToLower();
            //return result.ToPage((int)page, (int)pagesize)
            //    .Select(beer => beer.ToBeerDTO())
            //    .ToList();

            return order.ToLower() switch
            {
                "name_asc" => result.OrderBy(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(b => b.ToBeerDTO()).ToList(),
                "name_desc" => result.OrderByDescending(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(b => b.ToBeerDTO()).ToList(),
                _ => result.OrderBy(x => x.Name)
                    .ToPage((int)page, (int)pagesize)
                    .Select(b => b.ToBeerDTO()).ToList(),
            };

            ////TODO: TEST WHY SO SLOW
            //return order.ToLower() switch
            //{
            //    //"name" => result.OrderBy(x => x.Name, arrange)
            //    //    .ToPage((int)page, (int)pagesize)
            //    //    .Select(beer => beer.ToBeerDTO()).ToList(),
            //    "name" => result.OrderBy(x => x.Name)
            //        .ToPage((int)page, (int)pagesize)
            //        .Select(beer => beer.ToBeerDTO()).ToList(),

            //    "brewery" => result.OrderBy(x => x.Brewery.Name, arrange)
            //        .ToPage((int)page, (int)pagesize)
            //        .Select(beer => beer.ToBeerDTO()).ToList(),

            //    "style" => result.OrderBy(x => x.Style.Name, arrange)
            //        .ToPage((int)page, (int)pagesize)
            //        .Select(beer => beer.ToBeerDTO()).ToList(),

            //    "abv" => result.OrderBy(x => x.ABV, arrange)
            //        .ToPage((int)page, (int)pagesize)
            //        .Select(beer => beer.ToBeerDTO()).ToList(),

            //    "rating" => result.Select(beer => beer.ToBeerDTO())
            //        .OrderBy(x => x.AverageRating, arrange)
            //        .ToPage((int)page, (int)pagesize).ToList(),

            //    //_ => result.OrderBy(x => x.Name, arrange)
            //    //    .ToPage((int)page, (int)pagesize)
            //    //    .Select(beer => beer.ToBeerDTO()).ToList()
            //    _ => result.OrderBy(x => x.Name)
            //        .ToPage((int)page, (int)pagesize)
            //        .Select(beer => beer.ToBeerDTO()).ToList(),
            //};
        }

        public async Task<BeerDTO> Create(BeerDTO beerDTO)
        {
            if (beerDTO == null)
                throw new ArgumentNullException();

            var style = await this.beersContext.Styles
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Name.ToLower() == beerDTO.Style.ToLower());
            //add style if not existing
            if (style == null)
                style = await CreateStyle(beerDTO);

            var brewery = await this.beersContext.Breweries
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Name.ToLower() == beerDTO.Brewery.ToLower());
            //add brewery if not existing
            if (brewery == null)
                brewery = await CreateBrewery(beerDTO);

            Beer beer = new Beer()
            {
                ImageURL = beerDTO.ImageURL,
                Name = beerDTO.Name,
                Description = beerDTO.Description,
                ABV = beerDTO.ABV,

                Style = style,
                StyleId = style.Id,
                Brewery = brewery,
                BreweryId = brewery.Id
            };

            beer.CreatedOn = this.dateTimeProvider.GetDateTime();
            await this.beersContext.Beers.AddAsync(beer);
            await this.beersContext.SaveChangesAsync();

            return beerDTO;
        }



        public async Task<BeerDTO> Update(int id, BeerDTO beerDTO)
        {
            var beer = await this.beersContext.Beers
                .Include(s => s.Style)
                .Include(b => b.Brewery)
                .Include(x => x.BeerRatings)
                .Include(x => x.BeerReviews)
                .Where(beer => beer.IsDeleted == false)
                .FirstOrDefaultAsync(beer => beer.Id == id);

            if (beer == null)
                throw new ArgumentNullException();

            var style = await this.beersContext.Styles
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Name.ToLower() == beerDTO.Style.ToLower());
            //add style if not existing
            if (style == null)
                style = await CreateStyle(beerDTO);

            var brewery = await this.beersContext.Breweries
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Name.ToLower() == beerDTO.Brewery.ToLower());
            //add brewery if not existing
            if (brewery == null)
                brewery = await CreateBrewery(beerDTO);

            beer.Name = beerDTO.Name;
            beer.Description = beerDTO.Description;
            beer.ABV = beerDTO.ABV;
            beer.Style = style;
            beer.StyleId = style.Id;
            beer.Brewery = brewery;
            beer.BreweryId = brewery.Id;

            beer.ModifiedOn = this.dateTimeProvider.GetDateTime();

            await this.beersContext.SaveChangesAsync();
            return beerDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var beer = await this.beersContext.Beers
                    //.Where(x => x.IsDeleted == false)
                    .FirstOrDefaultAsync(x => x.Id == id);

                beer.IsDeleted = true;
                beer.DeletedOn = DateTime.Now;
                await this.beersContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public async Task AddReview(User user, int beerId, int rating, string text)
        {
            if (user == null)
                throw new ArgumentNullException();
            var beer = await this.beersContext.Beers
                .Include(x => x.BeerReviews)
                .FirstOrDefaultAsync(x => x.Id == beerId);

            if (beer == null)
                throw new ArgumentNullException();

            var review = new BeerReview()
            {
                Text = text,
                Rating = rating,
                BeerId = beer.Id,
                Beer = beer,
                UserId = user.Id,
                User = user,
                CreatedOn = DateTime.Now,
            };

            beer.BeerReviews.Add(review);
            await this.beersContext.Reviews.AddAsync(review);
            await this.beersContext.SaveChangesAsync();
        }


        //Helper methods
        public async Task<int> BeersCount()
        {
            return await this.beersContext.Beers.CountAsync();
        }

        public async Task<bool> BeerExists(string beerName)
        {
            var beer = await this.beersContext.Beers
                .Where(beer => beer.IsDeleted == false)
                .FirstOrDefaultAsync(beer => beer.Name.ToLower() == beerName.ToLower());

            return beer != null ? true : false;
        }







        private async Task<Brewery> CreateBrewery(BeerDTO beerDTO)
        {
            Brewery brewery = new Brewery()
            {
                Name = beerDTO.Brewery,
                CreatedOn = DateTime.Now,
            };
            await this.beersContext.Breweries.AddAsync(brewery);
            await this.beersContext.SaveChangesAsync();
            return brewery;
        }

        private async Task<Style> CreateStyle(BeerDTO beerDTO)
        {
            Style style = new Style()
            {
                Name = beerDTO.Style,
                CreatedOn = DateTime.Now,
            };
            await this.beersContext.Styles.AddAsync(style);
            await this.beersContext.SaveChangesAsync();

            return style;
        }
    }
}
