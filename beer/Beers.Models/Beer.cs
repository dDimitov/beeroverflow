﻿using Beers.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Beers.Models
{
    public class Beer : Entity
    {

        [Key]
        public int Id { get; set; }

        [Required, MaxLength(100), MinLength(2)]
        public string Name { get; set; }

        [MaxLength(500), MinLength(15)]
        public string Description { get; set; }
        public string ImageURL { get; set; }
        public int? StyleId { get; set; }
        public Style Style { get; set; }
        public int? BreweryId { get; set; }
        public Brewery Brewery { get; set; }

        [Range(0, 20)]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public double ABV { get; set; }
        public ICollection<BeerRating> BeerRatings { get; set; } = new List<BeerRating>();
        public ICollection<BeerReview> BeerReviews { get; set; } = new List<BeerReview>();
        public ICollection<AlreadyDrunkBeers> AlreadyDrunkBeers { get; set; } = new List<AlreadyDrunkBeers>();
    }
}
