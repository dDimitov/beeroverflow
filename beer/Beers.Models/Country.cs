﻿using Beers.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Beers.Models
{
    public class Country : Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MinLength(3), MaxLength(128)]
        public string Name { get; set; }
        public ICollection<User> Users { get; set; }
        public ICollection<Brewery> Breweries { get; set; }
    }
}
