﻿using Beers.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Beers.Models
{
    public class Brewery : Entity
    {
        public int Id { get; set; }
        [Required, MaxLength(250), MinLength(3)]
        public string Name { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }

        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}
