﻿using Beers.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Beers.Models
{
    public class Style : Entity
    {
        public int Id { get; set; }

        [Required, MinLength(3), MaxLength(100)]
        public string Name { get; set; }

        public ICollection<Beer> Beers { get; set; }
    }
}
