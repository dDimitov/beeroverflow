﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Beers.Models.Abstract
{
    public abstract class BeerFeedback : Entity
    {
        public int? BeerId { get; set; }
        [ForeignKey("BeerId")]
        public Beer Beer { get; set; }

        public int? UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}
