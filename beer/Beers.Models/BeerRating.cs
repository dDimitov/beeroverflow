﻿using Beers.Models.Abstract;
using System;
using System.ComponentModel.DataAnnotations;

namespace Beers.Models
{
    public class BeerRating : BeerFeedback
    {
        [Required]
        [Range(1, 5)]
        public int Rating { get; set; }
    }
}
