﻿using Beers.Models.Abstract;

namespace Beers.Models
{
    public class AlreadyDrunkBeers : Entity
    {
        public int BeerId { get; set; }
        public int UserId { get; set; }
        public Beer Beer { get; set; }
        public User User { get; set; }
    }
}
