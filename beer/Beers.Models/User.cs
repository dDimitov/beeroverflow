﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Beers.Models
{

    //this is the User Model

    public class User : IdentityUser<int>//Entity//, IdentityUser<int>
    {
        //[Key]
        //public int Id { get; set; }
        public virtual ICollection<WishListItem> WishList { get; set; } = new List<WishListItem>();
        public virtual ICollection<BeerReview> Reviews { get; set; } = new List<BeerReview>();
        public virtual ICollection<BeerRating> Ratings { get; set; } = new List<BeerRating>();
        public virtual ICollection<AlreadyDrunkBeers> AlreadyDrunkBeers { get; set; } = new List<AlreadyDrunkBeers>();

        [Required, MaxLength(15), MinLength(5)]
        public string Name { get; set; }

        //[Required, MaxLength(13), MinLength(8)]
        //[DataType(DataType.Password)]
        //public string Password { get; set; }

        //[EmailAddress]
        //public string Email { get; set; }
        //[Key]
        public int? CountryId { get; set; }
        //[ForeignKey("CountryId")]
        public Country Country { get; set; }

        public int Age { get; set; }
        public string ImageURL { get; set; }


        public bool IsBanned { get; set; }
        public bool IsAdmin { get; set; }



        //-----------------------------
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
