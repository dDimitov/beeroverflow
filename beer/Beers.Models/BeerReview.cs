﻿using Beers.Models.Abstract;
using System.ComponentModel.DataAnnotations;

namespace Beers.Models
{
    public class BeerReview : BeerFeedback
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MinLength(5), MaxLength(250)]
        public string Text { get; set; }

        [Required]
        [Range(1, 5)]
        public int Rating { get; set; }
        //public ICollection<ReviewLike> Likes { get; set; } = new List<ReviewLike>();
    }
}
