﻿using Beers.Data;
using Microsoft.EntityFrameworkCore;

namespace Beers.Test
{
    public class Utils
    {
        public static DbContextOptions<BeersContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<BeersContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}
