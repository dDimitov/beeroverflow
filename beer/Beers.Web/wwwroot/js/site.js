﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


$(window).on("scroll", function () {
    if ($(this).scrollTop() > window.innerHeight / 2) {
        $(".main-header").css({ "height": "0px" });
        $(".main-header").hide();//(".not-transparent");
        $(".top-header").hide();
        //document.getElementById("navbar").style.top = "0";
        $("#home-page-header #navbar").css("top", "0px");
    }
    else {
        $(".main-header").show();//(".not-transparent");
        $(".top-header").show();
        //document.getElementById("navbar").style.top = "-200px";

        $("#home-page-header #navbar").css("top", "-200px");
    }
});
$('.carousel').carousel({
    interval: 5000
});

$('.top-header #search-inp').on('keyup', function () {
    $('.top-header #search-btn').attr('href', 'Beers?search=' + $('.top-header #search-inp').val());
});

var $star_rating = $('.your-ratings .star-rating .fa');

var SetRatingStar = function() {
return $star_rating.each(function() {
    if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
        return $(this).addClass('checked');
    } else {
        return $(this).removeClass('checked');//.addClass('fa-star-o');
    }
});
};

$star_rating.on('click', function() {
$star_rating.siblings('input.rating-value').val($(this).data('rating'));
    return SetRatingStar();
});

SetRatingStar();

$(document).ready(function() {

});


//------------- AJAX REQUESTS --------------------


//ALREADY DRUNK ---------------------------------------------
function AddToAlreadyDrunk() {
    var beerId = $('#form-drunk input[name=beerId]').val();
    var data = { beerId };
    $.ajax({
        type: "POST",
        url: 'https://localhost:5001/Beers/AddToDrunkList',
        data: data,
        success: function (resultData) {
            $("#form-drunk > button").toggleClass("d-none");
            alertify.success('Added to Already Drunk'); 
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == "401" || jqXHR.status == 401) {
                window.location = 'https://localhost:5001/Account/Login';
            }
        }
    });
}

function RemoveFromAlreadyDrunk() {
    var beerId = $('#form-drunk input[name=beerId]').val();
    var data = { beerId };
    $.ajax({
        type: "POST",
        url: 'https://localhost:5001/Beers/RemoveFromDrunkList',
        data: data,
        success: function (resultData) {
            $("#form-drunk > button").toggleClass("d-none");
            alertify.error('Removed from Already Drunk'); 
        },
    });
}
//ALREADY DRUNK /END ---------------------------------------------

//WISHLIST ---------------------------------------------------------
function AddToWishList() {
    var beerId = $('#form-wishlist input[name=beerId]').val();
    var data = { beerId };
    $.ajax({
        type: "POST",
        url: 'https://localhost:5001/Beers/AddToWishList',
        data: data,
        success: function (resultData) {
            $("#form-wishlist > button").toggleClass("d-none");
            alertify.success('Added to wishlist'); 
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == "401" || jqXHR.status == 401) {
                window.location = 'https://localhost:5001/Account/Login';
            }
        }
    });
}

function RemoveFromWishList() {
    var beerId = $('#form-wishlist input[name=beerId]').val();
    var data = { beerId };
    $.ajax({
        type: "POST",
        url: 'https://localhost:5001/Beers/RemoveFromWishList',
        data: data,
        success: function (resultData) {
            $("#form-wishlist > button").toggleClass("d-none");
            alertify.error('Removed from wishlist'); 
        },
    });
}
//WISHLIST / END ---------------------------------------------------------

function AddReview() {
    var beerId = $('#form-add-review input[name=beerId]').val();
    var rating = $('#form-add-review input[name=rating]').val();
    var text = $('#form-add-review textarea').val();
    var data = { beerId, rating, text };
    $.ajax({
        type: "POST",
        url: 'https://localhost:5001/Beers/AddReview',
        data: data,
        success: function (resultData) {
            alertify.success('Review added'); 
            location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == "401" || jqXHR.status == 401) {
                window.location = 'https://localhost:5001/Account/Login';
            }
        }
    });
}


//DELETE BEER -------------------------------------------------------

function FastDelete(beerId) {
    alertify.confirm('Delete Beer #' + beerId, 'Are you sure you want to delete this beer?', function () {
        
        $.ajax({
            type: "POST",
            url: 'https://localhost:5001/Beers/Delete/' + beerId,
            success: function (resultData) {
                alertify.error('Deleted')
                location.reload();
            },
        });

    }, function () {
        alertify.error('Cancel')
    });
}

//DELETE BREWERY ----------------------------------------------------------

function FastDeleteBrewery(breweryId) {
    alertify.confirm('Delete Brewery #' + breweryId, 'Are you sure you want to delete this brewery?', function () {

        $.ajax({
            type: "POST",
            url: 'https://localhost:5001/Breweries/Delete/' + breweryId,
            success: function (resultData) {
                alertify.error('Deleted')
                location.reload();
            },
        });

    }, function () {
        alertify.error('Cancel')
    });
}

// DELETE COUNTRY -----------------------------

function FastDeleteCountry(countryId) {
    alertify.confirm('Delete country #' + countryId, 'Are you sure you want to delete this country?', function () {

        $.ajax({
            type: "POST",
            url: 'https://localhost:5001/Countries/Delete/' + countryId,
            success: function (resultData) {
                alertify.error('Deleted')
                location.reload();
            },
        });

    }, function () {
        alertify.error('Cancel')
    });
}


// DELETE STYLE ---------------------------------------------

function FastDeleteStyle(styleId) {
    alertify.confirm('Delete Style #' + styleId, 'Are you sure you want to delete this style?', function () {
        $.ajax({
            type: "POST",
            url: 'https://localhost:5001/Styles/Delete/' + styleId,
            success: function (resultData) {
                alertify.error('Deleted')
                location.reload();
            },
        });
    }, function () {
        alertify.error('Cancel')
    });
}

// DELETE USER ---------------------------------------------

function FastDeleteUser(userId) {
    alertify.confirm('Delete User #' + userId, 'Are you sure you want to delete this user?', function () {
        $.ajax({
            type: "POST",
            url: 'https://localhost:5001/Users/Delete/' + userId,
            success: function (resultData) {
                alertify.error('Deleted')
                location.reload();
            },
        });
    }, function () {
        alertify.error('Cancel')
    });
}


// PAGING -----------------------------------------------

$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
}

function MovePrevious() {
    var search = $.urlParam('search');
    var order = $.urlParam('order');
    var page = $.urlParam('page');

    var loacationWithoutQuery = window.location.href.split('?')[0];

    if (decodeURIComponent(page) == null || page == null || decodeURIComponent(page) == "1" || page == "1") {
        return;
    }
    else {
        console.log(decodeURIComponent(page));
        page = parseInt(decodeURIComponent(page)) - 1;
    }
    var params = {};

    if (search == null) {
        if (order == null) {
            params = {
                page: page
            }
        }
        else {
            params = {
                order: order,
                page: page
            }
        }
    }
    else {
        if (order == null) {
            params = {
                search: search,
                page: page
            }
        }
        else {
            params = {
                search: search,
                order: order,
                page: page
            }
        }
    }
    var str = jQuery.param(params);
    window.location.href = loacationWithoutQuery + '?' + str;
}

function MoveNext() {
    var search = $.urlParam('search');
    var order = $.urlParam('order');
    var page = $.urlParam('page');

    var loacationWithoutQuery = window.location.href.split('?')[0];

    //if (decodeURIComponent(page) == null || decodeURIComponent(page) == 1) {
    //    return;
    //}
    if (page == null) {
        console.log(decodeURIComponent(page));
        page = 2;
    }
    else {
        console.log(decodeURIComponent(page));
        page = parseInt(decodeURIComponent(page)) + 1;
    }
    var params = { };

    if (search == null) {
        if (order == null) {
            params = {
                page: page
            }
        }
        else {
            params = {
                order: order,
                page: page
            }
        }
    }
    else {
        if (order == null) {
            params = {
                search: search,
                page: page
            }
        }
        else {
            params = {
                search: search,
                order: order,
                page: page
            }
        }
    }
    var str = jQuery.param(params);
    window.location.href = loacationWithoutQuery + '?' + str;
}

function Search() {
    var search = $('.search-form #search-input').val();
    //var search = $.urlParam('search');
    if (!search) {
        return;
    }
    var order = $.urlParam('order');
    var page = $.urlParam('page');

    var loacationWithoutQuery = window.location.href.split('?')[0];

    page = 1;

    var params = {};

    if (order == null) {
        params = {
            search: search,
            page: page
        }
    }
    else {
        params = {
            search: search,
            order: order,
            page: page
        } 
    }
    var str = jQuery.param(params);
    window.location.href = loacationWithoutQuery + '?' + str;
}