﻿using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Beers.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryService countryService;
        public CountriesController(ICountryService countryService)
        {
            this.countryService = countryService;
        }
        //[Route("api/[controller]")]
        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] string order, [FromQuery] string search, [FromQuery] int? page)
        {
            int _pageSize = 10;
            IEnumerable<CountryDTO> countriesSource = await countryService.GetAllCountries(page, _pageSize, search);

            //var countries = countriesSource.AsQueryable();
            //int count = countries.Count();
            //int CurrentPage = pageNumber;
            //int PageSize = _pageSize;
            //int TotalCount = count;
            //int TotalPages = (int)Math.Ceiling(count / (double)PageSize);
            //var items = countries.Skip((CurrentPage - 1) * PageSize).Take(PageSize);
            //var previousPage = CurrentPage > 1;
            //var nextPage = CurrentPage < TotalPages;
            return Ok(countriesSource);
        }

        // GET api/<CountriesController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var country = await this.countryService.GetCountry(id);
                return Ok(country);
            }
            catch (Exception)
            {
                return NotFound();
                throw;
            }
        }

        // POST api/<CountriesController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateCountryViewModel model)
        {
            if (model == null)
                return BadRequest();

            var exists = await this.countryService.CountryExists(model.Name);
            if (exists)
                return StatusCode(409, $"Country '{model.Name}' already exists.");

            var countryDTO = new CountryDTO(model.Name);

            var country = await this.countryService.Create(countryDTO);

            return Created("post", country);
        }

        // PUT api/<CountriesController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CreateCountryViewModel model)
        {
            //TODO: Refactor this logic?
            if (id < 1 || model == null)
                return BadRequest();

            var modelDTO = new CountryDTO(model.Name);

            try
            {
                var updatedcountryDTO = await this.countryService.Update(id, modelDTO);
                return Ok(updatedcountryDTO);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        // DELETE api/<CountriesController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await this.countryService.Delete(id);

            if (result == true)
                return NoContent();
            else
                return BadRequest();
        }
    }
}
