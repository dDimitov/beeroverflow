﻿using Beers.Data;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Beers.Web.Models.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Beers.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeersController : ControllerBase
    {
        private readonly IBeerService beerService;

        public BeersController(IBeerService beerService, BeersContext beersContext)
        {
            this.beerService = beerService;
        }

        //TODO: add order
        //GET api/beers
        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] string order, [FromQuery] string search, [FromQuery] int? page)
        {
            int _pageSize = 10;

            IEnumerable<BeerDTO> beersSource = await this.beerService.GetAllBeers(page, _pageSize, search);
            //if (search != null)
            //{
            //    await this.beerService.GetAllBeers();
            //}
            //var beers = beersSource.AsQueryable();
            //int count = beers.Count();
            //int CurrentPage = pageNumber;
            //int PageSize = _pageSize;
            //int TotalCount = count;
            //int TotalPages = (int)Math.Ceiling(count / (double)PageSize);
            //var items = beers.Skip((CurrentPage - 1) * PageSize).Take(PageSize);
            //var previousPage = CurrentPage > 1;
            //var nextPage = CurrentPage < TotalPages;
            // Object which we are going to send in header   
            //var paginationMetadata = new
            //{
            //    totalCount = TotalCount,
            //    pageSize = PageSize,
            //    currentPage = CurrentPage,
            //    totalPages = TotalPages,
            //    previousPage,
            //    nextPage
            //};

            // Returing List of Customers Collections  
            return Ok(beersSource);
            //int pageSize = 3;
            //int pageNumber = (page ?? 1);
            ////this.beerService.GetAllBeers()
            ////return View(this.beersService.GetAllBeers().ToPagedList(pageNumber, pageSize));
            //var beers = new AsyncEnumerable(await this.beerService.GetAllBeers());

            //return Ok(await PageList<BeerDTO>.CreateAsync(beers., pageNumber, pageSize));//TODO: need test
            ////return Ok(await this.beerService
            ////    .GetAllBeers());
        }

        //GET api/beers/:id
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var beer = await this.beerService.GetBeer(id);
                return Ok(beer);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }



        //POST api/beers
        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] BeerViewModel model)
        {
            if (model == null)
                return BadRequest();

            var exists = await this.beerService.BeerExists(model.Name);
            if (exists)
                return StatusCode(409, $"Beer '{model.Name}' already exists.");

            BeerDTO beerDTO = model.ToBeerDTO();

            var beer = await this.beerService.Create(beerDTO);

            return Created("post", beer);
        }

        //PUT api/beers/:id
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] BeerViewModel model)
        {
            BeerDTO beerDTO = model.ToBeerDTO();

            try
            {
                var updatedBeerDTO = await this.beerService.Update(id, beerDTO);
                return Ok(updatedBeerDTO);
            }
            catch (Exception)
            {
                return BadRequest();
                throw;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await this.beerService.Delete(id);

            if (result == true)
                return NoContent();
            else
                return BadRequest();
        }
    }
}
