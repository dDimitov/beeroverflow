﻿using Beers.Services;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Beers.Web.Models.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Beers.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BreweriesController : ControllerBase
    {
        private readonly IBreweryService breweryService;
        //[Route("api/[controller]")]
        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] string order, [FromQuery] string search, [FromQuery] int? page)
        {
            int _pageSize = 10;
            IEnumerable<BreweryDTO> brewerySource = await breweryService.GetAllBreweries(page, _pageSize, search);
            //var breweries = brewerySource.AsQueryable();
            //int count = breweries.Count();
            //int CurrentPage = pageNumber;
            //int PageSize = _pageSize;
            //int TotalCount = count;
            //int TotalPages = (int)Math.Ceiling(count / (double)PageSize);
            //var items = breweries.Skip((CurrentPage - 1) * PageSize).Take(PageSize);
            //var previousPage = CurrentPage > 1;
            //var nextPage = CurrentPage < TotalPages;

            return Ok(brewerySource);

        }



        public BreweriesController(IBreweryService breweryService)
        {
            this.breweryService = breweryService;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var brewery = await this.breweryService.GetBrewery(id);
                return Ok(brewery);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateBreweryViewModel model)
        {
            if (model == null)
                return BadRequest();

            var exists = await this.breweryService.BreweryExists(model.Name, model.Country);
            if (exists)
                return StatusCode(409, $"Brewery '{model.Name}' already exists.");

            var modelDTO = new BreweryDTO(model.Name, model.Country);
            if (!await this.breweryService.CountryExists(model.Country))
            {
                await this.breweryService.CreateCountry(modelDTO);
            }

            var brewery = await this.breweryService.Create(modelDTO);

            return Created("post", brewery);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] BreweryViewModel model)
        {
            try
            {
                var updatedBreweryDTO = await this.breweryService.Update(id, model.ToBreweryDTO());
                return Ok(updatedBreweryDTO);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await this.breweryService.Delete(id);

            if (result == true)
                return NoContent();
            else
                return BadRequest();
        }
    }
}
