﻿using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Beers.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] string order, [FromQuery] string search, [FromQuery] int? page)
        {
            int _pageSize = 10;
            IEnumerable<UserDTO> userSource = await userService.GetAllUsers(page, _pageSize, search);

            //var users = userSource.AsQueryable();
            //int count = users.Count();
            //int CurrentPage = pageNumber;
            //int PageSize = _pageSize;
            //int TotalCount = count;
            //int TotalPages = (int)Math.Ceiling(count / (double)PageSize);
            //var items = users.Skip((CurrentPage - 1) * PageSize).Take(PageSize);
            //var previousPage = CurrentPage > 1;
            //var nextPage = CurrentPage < TotalPages;
            return Ok(userSource);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(int id)
        {
            try
            {
                var user = await this.userService.GetUser(id);
                return Ok(user);
            }
            catch (Exception)
            {
                return NotFound();
            }

        }

        // GET: api/Users/5
        //[HttpGet("{id}/wishlist")]
        //public async Task<ActionResult<User>> GetWishlist(int id)
        //{
        //    try
        //    {
        //        var user = await this.userService.GetWishList(id);
        //        return Ok(user);
        //    }
        //    catch (Exception)
        //    {
        //        return NotFound();
        //    }

        //}

        // PUT: api/Users/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //TODO: make with userViewModel
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] RegisterPageViewModel model)
        {
            UserDTO modelDTO = await GetUserDTO(model);
            try
            {
                var updated = await this.userService.Update(id, modelDTO);
                return Ok(updated);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        private async Task<UserDTO> GetUserDTO([FromBody] RegisterPageViewModel model)
        {
            return new UserDTO()
            {
                Name = model.Name,
                Password = model.Password,
                Email = model.Email,
                Country = model.Country,
                Age = model.Age,
                ImageURL = model.ImageURL
            };
        }

        // POST: api/Users
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<UserDTO>> Post([FromBody] RegisterPageViewModel model)
        {
            var userDTO = await GetUserDTO(model);

            var exists = await this.userService.UserExists(model.Name);
            if (exists)
                return StatusCode(409, $"User '{model.Name}' already exists.");

            var modelDTO = await GetUserDTO(model);

            var user = await this.userService.Create(userDTO);

            return Created("post", user);//CreatedAtAction("GetUser", new { id = user.Id }, user);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> Delete(int id)
        {
            var user = await this.userService.Delete(id);

            if (user == false)
                return NotFound();

            return NoContent();
        }

    }
}
