﻿using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Beers.Web.ApiControllers
{
    [Route("api/[controller]")]
    public class StylesController : ControllerBase
    {
        private readonly IStyleService styleService;
        public StylesController(IStyleService styleService)
        {
            this.styleService = styleService;
        }
        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] string order, [FromQuery] string search, [FromQuery] int? page)
        {
            int _pageSize = 10;
            IEnumerable<StyleDTO> styleSource = await styleService.GetAllStyles(page, _pageSize, search);

            //var styles = styleSource.AsQueryable();
            //int count = styles.Count();
            //int CurrentPage = pageNumber;
            //int PageSize = _pageSize;
            //int TotalCount = count;
            //int TotalPages = (int)Math.Ceiling(count / (double)PageSize);
            //var items = styles.Skip((CurrentPage - 1) * PageSize).Take(PageSize);
            //var previousPage = CurrentPage > 1;
            //var nextPage = CurrentPage < TotalPages;
            return Ok(styleSource);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var style = await this.styleService.GetStyle(id);
                return Ok(style);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateStyleViewModel model)
        {
            if (model == null)
                return BadRequest();

            var exists = await this.styleService.StyleExists(model.Name);
            if (exists)
                return StatusCode(409, $"Style '{model.Name}' already exists.");

            var modelDTO = new StyleDTO(model.Name);

            var style = await this.styleService.Create(modelDTO);

            return Created("post", style);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CreateStyleViewModel model)
        {
            //TODO: Refactor this logic?
            if (id < 1 || model == null)
                return BadRequest();

            var modelDTO = new StyleDTO(model.Name);

            try
            {
                var updatedStyleDTO = await this.styleService.Update(id, modelDTO);
                return Ok(updatedStyleDTO);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            //this.countryService.Delete(id);
            var result = await this.styleService.Delete(id);

            if (result == true)
                return NoContent();
            else
                return BadRequest();
        }
    }
}
