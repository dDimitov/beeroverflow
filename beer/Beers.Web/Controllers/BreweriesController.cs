using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Beers.Web.Models.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Controllers
{
    public class BreweriesController : Controller
    {
        private readonly ILogger<BreweriesController> _logger;
        private readonly IBreweryService _breweryService;

        public BreweriesController(ILogger<BreweriesController> logger, IBreweryService breweryService)
        {
            _logger = logger;
            _breweryService = breweryService;
        }

        [HttpPost]
        public async Task<IActionResult> Search(string searchText, string country)
        {
            var beers = await _breweryService.GetAllBreweries(filter: searchText, country: country);
            var model = beers.Select(x => x.ToBreweryVM());

            return View("Index", model);
        }

        // GET: BreweriesController.cs
        public async Task<IActionResult> Index([FromQuery] int? pagesize, [FromQuery] int? page, [FromQuery] string search, string order, string arrange)
        {
            var breweries = await _breweryService.GetAllBreweries(page, pagesize, search, order, arrange);
            var model = breweries.Select(x => x.ToBreweryVM());

            return View(model);
        }

        //TODO: add Detail View Model
        // GET: Breweries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            BreweryViewModel breweryVM;
            try
            {
                var brewery = await _breweryService.GetBrewery(id);
                breweryVM = brewery.ToBreweryVM();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(breweryVM);
        }
        // GET: BreweriesController.cs/Details/5

        // GET: BreweriesController.cs/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: BreweriesController.cs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateBreweryViewModel model)
        {
            if (model == null)
                return BadRequest();

            var exists = await _breweryService.BreweryExists(model.Name, model.Country);
            if (exists)
                return StatusCode(409, $"Brewery '{model.Name}' already exists.");

            BreweryDTO breweryDTO = model.ToBreweryDTO();

            await _breweryService.Create(breweryDTO);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        // GET: BreweriesController.cs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            EditBreweryViewModel editBreweryVM;
            try
            {
                var brewery = await _breweryService.GetBrewery(id);
                editBreweryVM = brewery.ToEditBreweryVM();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(editBreweryVM);
        }

        // POST: BreweriesController.cs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditBreweryViewModel model)
        {
            try
            {
                var brewery = model.ToBreweryDTO();
                await _breweryService.Update(id, brewery);
            }
            catch (Exception)
            {
                return NotFound();
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: BreweriesController.cs/Delete/5
        //[Route("Breweries/Delete/{id}")]
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    BreweryViewModel breweryVM;
        //    try
        //    {
        //        var brewery = await _breweryService.GetBrewery(id);
        //        breweryVM = brewery.ToBreweryVM();
        //    }
        //    catch (Exception)
        //    {
        //        return NotFound();
        //        throw;
        //    }

        //    return View(breweryVM);
        //}

        // POST: BreweriesController.cs/Delete/5
        [HttpPost, ActionName("Delete")]
        [Route("Breweries/Delete/{id}")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _breweryService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
