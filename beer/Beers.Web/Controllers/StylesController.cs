﻿using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Beers.Web.Models.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Controllers
{
    public class StylesController : Controller
    {
        private readonly ILogger<StylesController> _logger;
        private readonly IStyleService _styleService;
        private readonly UserManager<User> userManager;

        public StylesController(ILogger<StylesController> logger, IStyleService styleService, UserManager<User> userManager)
        {
            _logger = logger;
            _styleService = styleService;
            this.userManager = userManager;
        }


        [HttpPost]
        public async Task<IActionResult> Search(string searchText)
        {
            var beers = await _styleService.GetAllStyles(filter: searchText);
            var model = beers.Select(x => x.ToStyleVM());

            return View("Index", model);
        }

        // GET: Styles
        public async Task<IActionResult> Index([FromQuery] int? pagesize, [FromQuery] int? page, [FromQuery] string search, string order, string arrange)
        {
            var user = await this.userManager.GetUserAsync(User);
            var styles = await _styleService.GetAllStyles(page, pagesize, search, order, arrange);
            var model = styles.Select(x => x.ToStyleVM());

            return View(model);
        }

        //TODO: add Detail View Model
        // GET: Styles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            StyleViewModel styleVM;
            try
            {
                var style = await _styleService.GetStyle(id);
                styleVM = style.ToStyleVM();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(styleVM);
        }

        [Authorize(Roles = "Admin")]
        // GET: Styles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Styles/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateStyleViewModel model)
        {
            if (model == null)
                return BadRequest();

            var exists = await _styleService.StyleExists(model.Name);
            if (exists)
                return StatusCode(409, $"Style '{model.Name}' already exists.");

            StyleDTO styleDTO = model.ToStyleDTO();

            await _styleService.Create(styleDTO);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        // GET: Styles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            EditStyleViewModel editStyleVM;
            try
            {
                var style = await _styleService.GetStyle(id);
                editStyleVM = style.ToEditStyleVM();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(editStyleVM);
        }

        // POST: Styles/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditStyleViewModel model)
        {
            try
            {
                var brewery = model.ToStyleDTO();
                await _styleService.Update(id, brewery);
            }
            catch (Exception)
            {
                return NotFound();
            }
            return RedirectToAction(nameof(Index));
        }

        //// GET: Styles/Delete/5
        //[Authorize(Roles = "Admin")]
        //[Route("Styles/Delete/{id}")]
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    StyleViewModel styleVM;
        //    try
        //    {
        //        var style = await _styleService.GetStyle(id);
        //        styleVM = style.ToStyleVM();
        //    }
        //    catch (Exception)
        //    {
        //        return NotFound();
        //        throw;
        //    }

        //    return View(styleVM);
        //}

        // POST: Styles/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [Route("Styles/Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _styleService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
