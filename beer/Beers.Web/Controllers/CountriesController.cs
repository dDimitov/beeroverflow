using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Beers.Web.Models.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Controllers
{
    public class CountriesController : Controller
    {
        private readonly ILogger<CountriesController> _logger;
        private readonly ICountryService _countryService;

        public CountriesController(ILogger<CountriesController> logger, ICountryService countryService)
        {
            _logger = logger;
            _countryService = countryService;
        }

        [HttpPost]
        public async Task<IActionResult> Search(string searchText)
        {
            var beers = await _countryService.GetAllCountries(filter: searchText);
            var model = beers.Select(x => x.ToCountryVM());

            return View("Index", model);
        }

        // GET: Countries
        public async Task<IActionResult> Index([FromQuery] int? pagesize, [FromQuery] int? page, [FromQuery] string search, string order, string arrange)
        {
            var breweries = await _countryService.GetAllCountries(page, pagesize, search, order, arrange);
            var model = breweries.Select(x => x.ToCountryVM());

            return View(model);
        }

        // GET: Countries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            CountryViewModel countryVM;
            try
            {
                var country = await _countryService.GetCountry(id);
                countryVM = country.ToCountryVM();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(countryVM);
        }

        [Authorize(Roles = "Admin")]
        // GET: Countries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Countries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateCountryViewModel model)
        {
            if (model == null)
                return BadRequest();

            var exists = await _countryService.CountryExists(model.Name);
            if (exists)
                return StatusCode(409, $"Country '{model.Name}' already exists.");

            CountryDTO countryDTO = model.ToCountryDTO();

            await _countryService.Create(countryDTO);
            return RedirectToAction("Index");
        }

        // GET: Countries/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            EditCountryViewModel editCountryVM;
            try
            {
                var country = await _countryService.GetCountry(id);
                editCountryVM = country.ToEditCountryVM();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(editCountryVM);
        }

        // POST: BeersController/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditCountryViewModel model)
        {
            try
            {
                var country = model.ToCountryDTO();
                await _countryService.Update(id, country);
            }
            catch (Exception)
            {
                return NotFound();
            }
            return RedirectToAction(nameof(Index));
        }


        //// GET: Countries/Delete/5
        //[Route("Countries/Delete/{id}")]
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    CountryViewModel countryVM;
        //    try
        //    {
        //        var country = await _countryService.GetCountry(id);
        //        countryVM = country.ToCountryVM();
        //    }
        //    catch (Exception)
        //    {
        //        return NotFound();
        //        throw;
        //    }

        //    return View(countryVM);
        //}

        // POST: Countries/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [Route("Countries/Delete/{id}")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _countryService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
