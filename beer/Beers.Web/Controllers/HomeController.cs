﻿using Beers.Services.Contracts;
using Beers.Web.Models;
using Beers.Web.Models.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IBeerService beerService;
        private readonly IBreweryService _breweryService;
        private readonly IStyleService _styleService;

        public HomeController(ILogger<HomeController> logger, IBeerService beerService, IBreweryService breweryService, IStyleService styleService)
        {
            _logger = logger;
            this.beerService = beerService ?? throw new ArgumentNullException(nameof(beerService));
            _breweryService = breweryService;
            _styleService = styleService;
        }

        public async Task<IActionResult> Index()
        {
            var styles = await _styleService.GetTopStyles(4);
            var beers = await this.beerService.GetTopBeers();
            var breweries = await _breweryService.GetTopBreweries(4);

            var beersCount = await this.beerService.BeersCount();
            var stylesCount = await this._styleService.StylesCount();
            var breweriesCount = await _breweryService.BreweriesCount();

            var model = new HomeIndexViewModel
            {
                TopRatedStyles = styles
                    .Select(x => x.ToStyleVM())
                    .ToList(),
                TopRatedBeers = beers
                    .Select(x => x.ToBeerVM())
                    .ToList(),
                TopRatedBreweries = breweries
                    .Select(x => x.ToBreweryVM())
                    .ToList(),
                BeersCount = beersCount,
                StylesCount = stylesCount,
                BreweriesCount = breweriesCount,
            };
            _logger.LogInformation(string.Join(Environment.NewLine, model.TopRatedBeers.Select(x => x.Name)));
            return View(model);
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginPageViewModel model)
        {
            //...code to login user to application...
            //_logger.LogInformation(model.UserName + ", " + model.Password);
            return View(model);
        }

        //[HttpGet]
        //public ActionResult Login()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Login(LoginPageVM model)
        //{
        //    //...code to login user to application...
        //    return View(model);
        //}

        public IActionResult Privacy()
        {
            return View();
        }

        public async Task<IActionResult> TopBeers()
        {
            var beers = await this.beerService.GetAllBeers();//.ToList();

            return View(beers);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
