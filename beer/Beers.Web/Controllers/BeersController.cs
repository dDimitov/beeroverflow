﻿using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Beers.Web.Models.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Controllers
{
    public class BeersController : Controller
    {
        private readonly IBeerService beerService;
        private readonly UserManager<User> _userManager;

        public BeersController(IBeerService beerService, UserManager<User> userManager)
        {
            this.beerService = beerService;
            _userManager = userManager;
        }


        [Authorize]
        [HttpPost]
        public async Task AddReview(int beerId, int rating, string text)
        {
            var user = await _userManager.GetUserAsync(User);
            await this.beerService.AddReview(user, beerId, rating, text);
        }

        [Authorize]
        [HttpPost]
        public async Task AddToDrunkList(int beerId)
        {
            var user = await _userManager.GetUserAsync(User);
            await this.beerService.AddBeerToAlreadyDrunkList(user, beerId);
        }

        [Authorize]
        [HttpPost]
        public async Task RemoveFromDrunkList(int beerId)
        {
            var user = await _userManager.GetUserAsync(User);
            await this.beerService.RemoveBeerFromAlreadyDrunkList(user, beerId);
        }

        [Authorize]
        [HttpPost]
        public async Task AddToWishList(int beerId)
        {
            var user = await _userManager.GetUserAsync(User);
            await this.beerService.AddToWishList(user, beerId);
        }

        [Authorize]
        [HttpPost]
        public async Task RemoveFromWishList(int beerId)
        {
            var user = await _userManager.GetUserAsync(User);
            await this.beerService.RemoveBeerFromWishList(user, beerId);
        }

        [HttpPost]
        public async Task<IActionResult> Search(string searchText, string country, string style)
        {
            var beers = await this.beerService.GetAllBeers(filter: searchText, country: country, style: style);
            var model = beers.Select(x => x.ToBeerVM());

            return View("Index", model);
        }

        // GET: Beers
        public async Task<IActionResult> Index([FromQuery] int? pagesize, [FromQuery] int? page, [FromQuery] string search, string order, string arrange, string country, string style)
        {
            var beers = await this.beerService.GetAllBeers(page, pagesize, search, order, arrange, country, style);
            var model = beers.Select(x => x.ToBeerVM());

            return View(model);
        }

        //TODO: add Detail View Model
        // GET: Beers/Details/5
        public async Task<IActionResult> Details(int id)
        {
            BeerViewModel beerVM;
            try
            {
                var beer = await this.beerService.GetBeer(id);
                beerVM = beer.ToBeerVM();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(beerVM);
        }

        [Authorize]
        // GET: Beers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Beers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateBeerViewModel model)
        {
            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri("http://localhost:5000/Beers/Create");

            //    //HTTP POST
            //    //await client.PostAsync(client.BaseAddress, model);
            //    client.PostAsync("http://localhost:5000/Beers/Create", )
            //    var postTask = client.PostAsJsonAsync<CreateBeerViewModel>("beer", model);
            //    postTask.Wait();

            //    var result = postTask.Result;
            //    if (result.IsSuccessStatusCode)
            //    {
            //        return RedirectToAction("Index");
            //    }
            //}

            if (model == null)
                return BadRequest();

            var exists = await this.beerService.BeerExists(model.Name);
            if (exists)
                return StatusCode(409, $"Beer '{model.Name}' already exists.");

            BeerDTO beerDTO = model.ToBeerDTO();

            await this.beerService.Create(beerDTO);
            return RedirectToAction("Index");
        }

        // GET: Beers/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            EditBeerViewModel beerVM;
            try
            {
                var beer = await this.beerService.GetBeer(id);
                beerVM = beer.ToEditBeerVM();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(beerVM);
        }

        // POST: Beers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditBeerViewModel model)
        {
            //if (ModelState.IsValid)
            //{
            try
            {
                var beer = model.ToBeerDTO();
                await this.beerService.Update(id, beer);
            }
            catch (Exception)
            {
                return NotFound();
            }
            return RedirectToAction(nameof(Index));
            //}
            //ViewData["BreweryId"] = new SelectList(_context.Breweries, "Id", "Name", beer.BreweryId);
            //ViewData["StyleId"] = new SelectList(_context.Styles, "Id", "Name", beer.StyleId);
            //return View(model);
        }

        // GET: Beers/Delete/5
        //[HttpGet]
        //[Route("Beers/Delete/{id}")]
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    BeerViewModel beerVM;
        //    try
        //    {
        //        var beer = await this.beerService.GetBeer(id);
        //        beerVM = beer.ToBeerVM();
        //    }
        //    catch (Exception)
        //    {
        //        return NotFound();
        //        throw;
        //    }

        //    return View(beerVM);
        //}

        // POST: Beers/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost]//, ActionName("Delete")]
        [Route("Beers/Delete/{id}")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            //var beer = await _context.Beers.FindAsync(id);
            //_context.Beers.Remove(beer);
            //await _context.SaveChangesAsync();
            try
            {
                await this.beerService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        //private bool BeerExists(int id)
        //{
        //    return _context.Beers.Any(e => e.Id == id);
        //}
    }
}
