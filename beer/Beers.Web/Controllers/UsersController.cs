﻿using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Beers.Web.Models.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Controllers
{
    public class UsersController : Controller
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IUserService _userService;
        private readonly UserManager<User> userManager;

        public UsersController(ILogger<UsersController> logger, IUserService userService, UserManager<User> userManager)
        {
            _logger = logger;
            _userService = userService;
            this.userManager = userManager;
        }

        // GET: Users
        public async Task<IActionResult> Index([FromQuery] int? pagesize, [FromQuery] int? page, [FromQuery] string search)
        {
            var users = await _userService.GetAllUsers(page, pagesize, search);
            var model = users.Select(x => x.ToUserVM()).ToList();

            return View(model);
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            UserViewModel user;
            try
            {
                var style = await _userService.GetUser(id);
                user = style.ToUserVM();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RegisterPageViewModel model)
        {
            if (model == null)
                return BadRequest();

            var exists = await _userService.UserExists(model.Name);
            if (exists)
                return StatusCode(409, $"User '{model.Name}' already exists.");

            UserDTO userDTO = model.ToUserDTO();

            await _userService.Create(userDTO);
            return RedirectToAction("Index");
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            EditUserViewModel editStyleVM;
            try
            {
                var style = await _userService.GetUser(id);
                editStyleVM = style.ToEditUserVM();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(editStyleVM);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditUserViewModel model)
        {
            try
            {
                var user = model.ToUserDTO();
                await _userService.Update(id, user);
            }
            catch (Exception)
            {
                return NotFound();
            }
            return RedirectToAction(nameof(Index));
        }


        //// GET: Users/Delete/5
        //[Route("Users/Delete/{id}")]
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    UserViewModel userVM;
        //    try
        //    {
        //        var user = await _userService.GetUser(id);
        //        userVM = user.ToUserVM();
        //    }
        //    catch (Exception)
        //    {
        //        return NotFound();
        //        throw;
        //    }

        //    return View(userVM);
        //}

        // POST: Users/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("Users/Delete/{id}")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                //var user = await this.userManager.FindByIdAsync(id.ToString());
                //user.IsDeleted = true;
                //await this.userManager.DeleteAsync(user);
                await _userService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

       
    }
}
