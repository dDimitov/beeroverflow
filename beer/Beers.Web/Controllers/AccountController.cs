﻿using Beers.Models;
using Beers.Services;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Beers.Web.Models.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly ICountryService _countryService;
        private readonly IUserService userService;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, ICountryService countryService, IUserService userService)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            _countryService = countryService;
            this.userService = userService;
        }

        [HttpGet]
        public IActionResult Register()
        {
            var vm = new RegisterPageViewModel();
            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterPageViewModel model)
        {
            if (ModelState.IsValid)
            {
                //var user = model.ToUser();//new User { UserName = model.Email, Email = model.Email };
                var user = new User()
                {
                    Name = model.Name,
                    UserName = model.Name,
                    Email = model.Email,
                    Age = model.Age,
                    CreatedOn = DateTime.Now,
                };
                if (!await _countryService.CountryExists(model.Country))
                {
                    var countryDTO = new CountryDTO(model.Country);
                    countryDTO = await _countryService.Create(countryDTO);
                    var country = countryDTO.ToCountry();

                    //user.Country = country;
                    user.CountryId = country.Id;
                }
                else
                {
                    var countryDTO = await _countryService.GetCountry(model.Country);
                    var country = countryDTO.ToCountry();

                    //user.Country = country;
                    user.CountryId = country.Id;
                }

                var result = await this.userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await this.signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }
            }

            return RedirectToAction("Error", "Home");
        }


        [HttpGet]
        public IActionResult Login()
        {
            //var vm = new LoginViewModel();
            //return View(vm);

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginPageViewModel model)
        {
            if (ModelState.IsValid)
            {
                //var user = userManager.GetUserAsync()
                var result = await this.signInManager.PasswordSignInAsync(model.UserName, model.Password, isPersistent: true, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            return RedirectToAction("Error", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> Logout(string returnUrl)
        {
            await this.signInManager.SignOutAsync();
            //_logger.LogInformation("User logged out.");

            if (!string.IsNullOrEmpty(returnUrl))
            {
                return RedirectToAction(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize]
        [HttpGet]
        [Route("Account/WishList")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> WishList()
        {
            var user = await this.userManager.GetUserAsync(User);

            var wishlist = await this.userService.GetWishList(user);

            //var wishlist = user.WishList
            //    .Select(x => x.Beer)
            //    .Select(x => x.ToBeerDTO().ToBeerVM())
            //    .ToList();

            //return RedirectToAction(nameof(Index));
            var result = wishlist.Select(x => x.ToBeerVM()).ToList();
            return View(result);
        }

        [Authorize]
        [HttpGet]
        [Route("Account/AlreadyDrunkList")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AlreadyDrunkList()
        {
            var user = await this.userManager.GetUserAsync(User);

            var wishlist = await this.userService.GetAlreadyDrunkList(user);

            //var wishlist = user.WishList
            //    .Select(x => x.Beer)
            //    .Select(x => x.ToBeerDTO().ToBeerVM())
            //    .ToList();

            //return RedirectToAction(nameof(Index));
            var result = wishlist.Select(x => x.ToBeerVM()).ToList();
            return View(result);
        }

        //// GET: Users/Edit/5
        //public async Task<IActionResult> Edit()
        //{
        //    EditUserViewModel editUserVM;
        //    try
        //    {
        //        var user = await this.userManager.GetUserAsync(User);//.GetUser(id);
        //        editUserVM = user.ToUserDTO().ToEditUserVM();
        //    }
        //    catch (Exception)
        //    {
        //        return NotFound();
        //    }

        //    return View(editUserVM);
        //}

        //// POST: Users/Edit/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for 
        //// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, EditUserViewModel model)
        //{
        //    try
        //    {
        //        var user = model.ToUserDTO();
        //        await await this.userManager.up//.Update(id, user);
        //    }
        //    catch (Exception)
        //    {
        //        return NotFound();
        //    }
        //    return RedirectToAction(nameof(Index));
        //}
    }
}
