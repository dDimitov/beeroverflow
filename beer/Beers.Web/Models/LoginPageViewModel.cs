﻿using System.ComponentModel.DataAnnotations;

namespace Beers.Web.Models
{
    public class LoginPageViewModel
    {
        [Required(ErrorMessage = "Are you really trying to login without entering username?")]
        [Display(Name = "Username")]
        //[EmailAddress]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Please enter password:)")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Name = "Stay logged in when browser is closed")]
        public bool RememberMe { get; set; }
    }
}
