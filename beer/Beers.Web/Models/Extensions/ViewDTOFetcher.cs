﻿using Beers.Models;
using Beers.Services.DTOs;
using System.Linq;

namespace Beers.Web.Models.Extensions
{
    public static class ViewDTOFetcher
    {
        public static User ToUser(this RegisterPageViewModel model)//, BeersContext beersContext)
        {
            return new User()
            {
                Name = model.Name,
                //PasswordHash = 
                //Password = model.Password,
                Email = model.Email,
                Age = model.Age,
                //Country = model.Country,
                ImageURL = model.ImageURL,
            };
        }
        public static UserDTO ToUserDTO(this EditUserViewModel model)//, BeersContext beersContext)
        {
            return new UserDTO()
            {
                Id = model.Id,
                Name = model.Name,
                Password = model.Password,
                Email = model.Email,
                Age = model.Age,
                Country = model.Country,
                ImageURL = model.ImageURL,
            };
        }

        public static EditUserViewModel ToEditUserVM(this UserDTO dto)
        {
            return new EditUserViewModel()
            {
                Id = dto.Id,
                Name = dto.Name,
                Password = dto.Password,
                Email = dto.Email,
                Age = dto.Age,
                Country = dto.Country,
                ImageURL = dto.ImageURL,
            };
        }

        public static UserDTO ToUserDTO(this RegisterPageViewModel model)//, BeersContext beersContext)
        {
            return new UserDTO()
            {
                Name = model.Name,
                Password = model.Password,
                Email = model.Email,
                Age = model.Age,
                Country = model.Country,
                ImageURL = model.ImageURL,
            };
        }

        public static UserViewModel ToUserVM(this UserDTO dto)
        {
            return new UserViewModel()
            {
                Id = dto.Id,
                //Name = dto.Name,
                Name = dto.UserName,
                Password = dto.Password,
                Email = dto.Email,
                Country = dto.Country,
                Age = dto.Age,
                ImageURL = dto.ImageURL
            };
        }

        public static StyleDTO ToStyleDTO(this EditStyleViewModel model)//, BeersContext beersContext)
        {
            return new StyleDTO()
            {
                Id = model.Id,
                Name = model.Name,
            };
        }

        public static EditStyleViewModel ToEditStyleVM(this StyleDTO dto)
        {
            return new EditStyleViewModel()
            {
                Id = dto.Id,
                Name = dto.Name,
            };
        }

        public static StyleDTO ToStyleDTO(this CreateStyleViewModel model)//, BeersContext beersContext)
        {
            return new StyleDTO()
            {
                Name = model.Name,
            };
        }

        public static StyleViewModel ToStyleVM(this StyleDTO dto)
        {
            return new StyleViewModel()
            {
                Id = dto.Id,
                Name = dto.Name,
                Beers = dto.Beers.Select(x => x.ToBeerVM()).ToList(),
            };
        }

        public static CountryDTO ToCountryDTO(this EditCountryViewModel model)//, BeersContext beersContext)
        {
            return new CountryDTO()
            {
                Id = model.Id,
                Name = model.Name,
            };
        }

        public static EditCountryViewModel ToEditCountryVM(this CountryDTO dto)
        {
            return new EditCountryViewModel()
            {
                Id = dto.Id,
                Name = dto.Name,
            };
        }

        public static CountryDTO ToCountryDTO(this CreateCountryViewModel model)//, BeersContext beersContext)
        {
            return new CountryDTO()
            {
                Name = model.Name,
            };
        }

        public static CountryViewModel ToCountryVM(this CountryDTO dto)
        {
            if (dto == null)
            {
                return null;
            }
            return new CountryViewModel()
            {
                Id = dto.Id,
                Name = dto.Name,
                Breweries = dto.Breweries.Select(x => x.ToBreweryVM()).ToList()
            };
        }

        public static BreweryViewModel ToBreweryVM(this BreweryDTO dto)
        {
            return new BreweryViewModel()
            {
                Id = dto.Id,
                Name = dto.Name,
                Country = dto.Country.ToCountryVM(),
                Beers = dto.Beers.Select(x => x.ToBeerVM()).ToList()
            };
        }

        public static EditBreweryViewModel ToEditBreweryVM(this BreweryDTO dto)
        {
            return new EditBreweryViewModel()
            {
                Id = dto.Id,
                Name = dto.Name,
                //Country = dto.Country.ToCountryVM(),
                Country = dto.Country.Name,
            };
        }

        public static BreweryDTO ToBreweryDTO(this EditBreweryViewModel model)//, BeersContext beersContext)
        {
            return new BreweryDTO()
            {
                Id = model.Id,
                Name = model.Name,
                //Country = model.Country,
                Country = new CountryDTO(model.Country),
            };
        }

        public static BreweryDTO ToBreweryDTO(this CreateBreweryViewModel model)//, BeersContext beersContext)
        {
            return new BreweryDTO()
            {
                Name = model.Name,
                //Country = model.Country,
                Country = new CountryDTO(model.Country),//model.Country.ToCountryDTO(),
            };
        }

        public static BeerViewModel ToBeerVM(this BeerDTO dto)
        {
            return new BeerViewModel()
            {
                Id = dto.Id,
                Name = dto.Name,
                Description = dto.Description,
                ABV = dto.ABV,
                Style = dto.Style,
                Brewery = dto.Brewery,
                ImageURL = dto.ImageURL,
                Reviews = dto.Reviews,
                AverageRating = dto.AverageRating,
            };
        }

        public static EditBeerViewModel ToEditBeerVM(this BeerDTO dto)//, BeersContext beersContext)
        {
            return new EditBeerViewModel()//beersContext
            {
                Id = dto.Id,
                Name = dto.Name,
                Description = dto.Description,
                ABV = dto.ABV,
                Style = dto.Style,
                Brewery = dto.Brewery,
                ImageURL = dto.ImageURL
            };
        }

        public static BeerDTO ToBeerDTO(this BeerViewModel model)
        {
            return new BeerDTO()
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                ABV = model.ABV,

                Style = model.Style,
                Brewery = model.Brewery,
                ImageURL = model.ImageURL,

                AverageRating = model.AverageRating,
            };
        }

        public static BeerDTO ToBeerDTO(this CreateBeerViewModel model)
        {
            return new BeerDTO()
            {
                Name = model.Name,
                Description = model.Description,
                ABV = model.ABV,
                Style = model.Style,
                Brewery = model.Brewery,
                ImageURL = model.ImageURL,

            };
        }

        public static BeerDTO ToBeerDTO(this EditBeerViewModel model)//, BeersContext beersContext)
        {
            return new BeerDTO()//beersContext
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                ABV = model.ABV,
                Style = model.Style,
                Brewery = model.Brewery,
                ImageURL = model.ImageURL,

            };
        }

        public static BreweryDTO ToBreweryDTO(this BreweryViewModel model)
        {
            return new BreweryDTO()
            {
                Id = model.Id,
                Name = model.Name,
                Country = model.Country.ToCountryDTO(),
                Beers = model.Beers.Select(x => x.ToBeerDTO()).ToList()

            };
        }

        public static CountryDTO ToCountryDTO(this CountryViewModel model)
        {
            return new CountryDTO()
            {
                Id = model.Id,
                Name = model.Name,
                Breweries = model.Breweries.Select(x => x.ToBreweryDTO()).ToList(),
            };
        }
    }
}

