﻿using System.ComponentModel.DataAnnotations;

namespace Beers.Web.Models
{
    public class RegisterPageViewModel
    {

        [Display(Name = "Username")]
        [Required(ErrorMessage = "Username field is required")]
        [MinLength(5, ErrorMessage = "Username cannot be less than 5")]
        [MaxLength(15, ErrorMessage = "Username cannot be more than 15")]
        public string Name { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Password field is required")]
        [DataType(DataType.Password)]
        [MinLength(4, ErrorMessage = "Password cannot be less than 4")]
        [MaxLength(100, ErrorMessage = "Password cannot be more than 100")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Required(ErrorMessage = "E-mail required")]
        public string Email { get; set; }

        [Display(Name = "Your Age")]
        [Required(ErrorMessage = "Age field required")]
        public int Age { get; set; }

        [DataType(DataType.ImageUrl)]
        [Display(Name = "Upload Image")]
        public string ImageURL { get; set; }

        [Display(Name = "Country")]
        [Required(ErrorMessage = "Country required")]
        [MinLength(3, ErrorMessage = "Country cannot be less than 3")]
        [MaxLength(128, ErrorMessage = "Country cannot be more than 128")]
        public string Country { get; set; }

    }
}
