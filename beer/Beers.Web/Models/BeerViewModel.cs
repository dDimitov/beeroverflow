﻿using Beers.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Beers.Web.Models
{
    public class BeerViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public double ABV { get; set; }
        public string Style { get; set; }
        public string Brewery { get; set; }
        public string ImageURL { get; set; }
        public List<BeerReview> Reviews { get; set; } = new List<BeerReview>();
        public double AverageRating { get; set; }
    }
}
