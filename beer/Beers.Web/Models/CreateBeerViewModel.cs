﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Beers.Web.Models
{
    public class CreateBeerViewModel
    {
        [Display(Name = "Beer Name")]
        [Required(ErrorMessage = "Beer name required")]
        [MinLength(2, ErrorMessage = "Beer name cannot be less than 2")]
        [MaxLength(100, ErrorMessage = "Brewery name cannot be more than 100")]
        public string Name { get; set; }

        [Display(Name = "Style")]
        [Required(ErrorMessage = "Style required")]
        [MinLength(2, ErrorMessage = "Style cannot be less than 2")]
        [MaxLength(100, ErrorMessage = "Style cannot be more than 100")]
        public string Style { get; set; }

        [Display(Name = "Description")]
        [MinLength(15, ErrorMessage = "Description cannot be less than 15")]
        [MaxLength(500, ErrorMessage = "Description cannot be more than 500")]
        public string Description { get; set; }

        [Display(Name = "Alcohol by volume")]
        [Range(0, 20)]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public double ABV { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ImageURL { get; set; }

        [Display(Name = "Country")]
        [MinLength(3, ErrorMessage = "Country name cannot be less than 3")]
        [MaxLength(100, ErrorMessage = "Country name cannot be more than 100")]
        public string Country { get; set; }

        [Display(Name = "Brewery")]
        [MinLength(3, ErrorMessage = "Brewery name cannot be less than 3")]
        [MaxLength(250, ErrorMessage = "Brewery name cannot be more than 100")]
        public string Brewery { get; set; }

    }
}
