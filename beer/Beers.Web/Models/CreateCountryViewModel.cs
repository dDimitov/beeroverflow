﻿using System.ComponentModel.DataAnnotations;

namespace Beers.Web.Models
{
    public class CreateCountryViewModel
    {
        [Display(Name = "Country Name")]
        [Required(ErrorMessage = "Country name required")]
        [MinLength(3, ErrorMessage = "Name cannot be less than 3")]
        [MaxLength(50, ErrorMessage = "Name cannot be more than 50")]
        public string Name { get; set; }
    }
}
