﻿namespace Beers.Web.Models
{
    public class EditCountryViewModel : CreateCountryViewModel
    {
        public int Id { get; set; }
    }
}
