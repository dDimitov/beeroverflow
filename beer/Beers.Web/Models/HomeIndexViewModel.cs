﻿using System.Collections.Generic;

namespace Beers.Web.Models
{
    public class HomeIndexViewModel
    {
        public List<StyleViewModel> TopRatedStyles { get; set; }
        public List<BeerViewModel> TopRatedBeers { get; set; }
        public List<BreweryViewModel> TopRatedBreweries { get; set; }
        public int BeersCount { get; set; }
        public int BreweriesCount { get; set; }
        public int StylesCount { get; set; }
    }
}
