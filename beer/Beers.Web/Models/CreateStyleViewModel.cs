﻿using System.ComponentModel.DataAnnotations;

namespace Beers.Web.Models
{
    public class CreateStyleViewModel
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "A Name is required")]
        [MinLength(3, ErrorMessage = "Style name cannot be less than 3")]
        [MaxLength(100, ErrorMessage = "Style name cannot be more than 100")]
        public string Name { get; set; }
    }
}