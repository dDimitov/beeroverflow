﻿using System.Collections.Generic;

namespace Beers.Web.Models
{
    public class CountryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<BreweryViewModel> Breweries { get; set; } = new List<BreweryViewModel>();
    }
}
