﻿using System.ComponentModel.DataAnnotations;

namespace Beers.Web.Models
{
    public class CreateBreweryViewModel
    {
        [Display(Name = "Brewery Name")]
        [Required(ErrorMessage = "Brewery name required")]
        [MinLength(3, ErrorMessage = "Brewery name cannot be less than 3")]
        [MaxLength(50, ErrorMessage = "Brewery name cannot be more than 50")]
        public string Name { get; set; }

        [Display(Name = "Country")]
        [Required(ErrorMessage = "Country name required")]
        [MinLength(3, ErrorMessage = "Brewery country cannot be less than 3")]
        [MaxLength(100, ErrorMessage = "Brewery country cannot be more than 100")]
        public string Country { get; set; }
    }
}
