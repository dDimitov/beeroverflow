﻿namespace Beers.Web.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        //public ICollection<WishListItem> WishList { get; set; }
        //public ICollection<AlreadyDrunkBeers> AlreadyDrunkBeers { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
        public int Age { get; set; }
        public string ImageURL { get; set; }
        public bool IsBanned { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsDeleted { get; set; }
    }
}
