﻿namespace Beers.Web.Models
{
    public class EditStyleViewModel : CreateStyleViewModel
    {
        public int Id { get; set; }
    }
}
