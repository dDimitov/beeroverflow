﻿using System.Collections.Generic;

namespace Beers.Web.Models
{
    public class StyleViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<BeerViewModel> Beers { get; set; } = new List<BeerViewModel>();
    }
}
