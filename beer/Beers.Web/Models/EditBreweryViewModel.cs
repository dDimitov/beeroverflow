﻿namespace Beers.Web.Models
{
    public class EditBreweryViewModel : CreateBreweryViewModel
    {
        public int Id { get; set; }
    }
}
