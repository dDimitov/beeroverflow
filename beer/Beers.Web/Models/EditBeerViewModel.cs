﻿namespace Beers.Web.Models
{
    public class EditBeerViewModel : CreateBeerViewModel
    {
        public int Id { get; set; }
    }
}
